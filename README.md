# GLProject

## ⚠️ Important

The "cloth rendering" example will be rendered only in commit `2729872ecf37bbd3c50defa738b93f8636d79dfd`.

The "LEADR" example will be rendered only in branch `master`.

## TL;DR

This project contains a part of "Real-time Cloth Rendering with Fiber-level
Detail" implementation coupled with a "LEADR Mapping" implementation.

## Download and run

To run the program, please operate the following commands:

```bash
git clone --recurse-submodules https://gitlab.com/paulracaniere/glproject
cd glproject
git checkout <master-or-commit-id>
mkdir build
cd build
cmake ..
make
cd ..
build/test_GL
```

## Resources link

The resources being too heavy for this repository can be found on:

https://drive.google.com/open?id=1EKMxqEQxB6iUrhoLwNJmwF_ece40B34M

## LEADR more info

The user can manipulate the model with the mouse.

Several number keys can be pressed on the keypad to change the shading

-> See fragment shader

The tessellation can be controlled with up/down keys.

The height can be controlled with right/left keys.

The width of the mesh can be controlled with `ctrl+left_drag`

If zooming with the mouse wheel is too slow, you may use the middle mutton and drag up and down.

## Contact

Contact `racanierepaul at gmail.com` for any problem.
