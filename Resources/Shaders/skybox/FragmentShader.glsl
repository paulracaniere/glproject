#version 410 core // Minimal GL version support expected from the GPU

// Shader inputs, linearly interpolated by default from the previous stage
in vec3 textureDir;

uniform samplerCube cubemap;

// Shader output: the color response attached to this fragment
out vec4 colorResponse;

void main() {
    colorResponse = texture(cubemap, textureDir);
}