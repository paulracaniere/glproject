#version 410 core // Minimal GL version support expected from the GPU

layout(location=0) in vec3 vPosition;

uniform mat4 projectionMat, viewMat;

out vec3 textureDir;

void main() {
    textureDir = vPosition;
    gl_Position = projectionMat * viewMat * vec4(vPosition, 1.0f);
}