#version 410 core // Minimal GL version support expected from the GPU

layout(location=0) in vec3 vPosition;// The 1st input attribute is the position (CPU side: glVertexAttrib 0)
layout(location=1) in vec3 vNormal;
layout(location=2) in vec3 vTangent;
layout(location=3) in vec3 vBinormal;
layout(location=4) in vec2 vTexCoord;

uniform mat4 projectionMat, modelViewMat, normalMat;

out vec3 fPosition;
out vec3 fNormal;
out vec3 fTangent;
out vec3 fBinormal;
out vec2 fTexCoord;

void main() {
    vec4 p = modelViewMat * vec4 (vPosition, 1.0f);
    gl_Position =  projectionMat * p;// mandatory to fire rasterization properly
    fPosition = p.xyz;
    fNormal = normalize((normalMat * vec4 (vNormal, 1.0f)).xyz);
    fTangent = normalize((normalMat * vec4(vTangent, 1.0f)).xyz);
    fBinormal = normalize((normalMat * vec4(vBinormal, 1.0f)).xyz);
    fTexCoord = vTexCoord;
}