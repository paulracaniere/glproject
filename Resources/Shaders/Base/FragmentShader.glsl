#version 410 core // Minimal GL version support expected from the GPU
#define M_PI 3.1415926535897932384626433832795

// Shader inputs, linearly interpolated by default from the previous stage
in vec3 fPosition;
in vec3 fNormal;
in vec3 fTangent;
in vec3 fBinormal;
in vec2 fTexCoord;

uniform sampler2D diffuseTex, normalTex, specularTex;

uniform mat4 projectionMat, modelViewMat, normalMat;

// Shader output: the color response attached to this fragment
out vec4 colorResponse;

void chessAlbedo(in vec2 texCoord, out vec3 albedo) {
    if ((int(texCoord[0] * 10) % 2 == 1 && int(texCoord[1] * 10) % 2 == 0) ||
    (int(texCoord[0] * 10) % 2 == 0 && int(texCoord[1] * 10) % 2 == 1)) albedo = vec3(1.0f);
    else albedo = vec3(1.0f, 0.0f, 0.0f);

}

void main() {
//    vec3 normalInTangentSpace = 2 * texture(normalTex, fTexCoord).xyz - vec3(1.0);
//    vec3 n = normalize(
//    normalInTangentSpace[2] * normalize(fNormal) +
//    normalInTangentSpace[1] * normalize(fTangent) +
//    normalInTangentSpace[0] * normalize(fBinormal));

    vec3 n = fNormal;

    //TODO: Make light as object
    vec3 lPosition = vec3(10.0f, 20.0f, 10.0f);
    lPosition = (vec4(lPosition, 1.0)).xyz;
    //    lPosition = (modelViewMat * vec4(lPosition, 1.0)).xyz;

    // Light incident vector
    vec3 wi = normalize(lPosition - fPosition);
    // Outgoing ray
    vec3 wo = normalize(-fPosition);
    // Half angle
    vec3 wh = normalize(wi + wo);

    vec3 albedo;
    chessAlbedo(fTexCoord, albedo);

    float diffuseIntensity = clamp(dot(n, wi), 0.0f, 1.0f);
    float specularIntensity = pow(clamp(dot(n, wh), 0.0f, 1.0f), 64);
    float ambientIntensity = 0.05f;

//    vec4 diffuseColor = texture(diffuseTex, fTexCoord);
    vec4 diffuseColor = vec4(190.0f, 144.0f, 87.0f, 255.0f) / 255.0f;
    vec4 specularColor = texture(specularTex, fTexCoord);
    //    vec4 specularColor = vec4(1.0);

    colorResponse = diffuseColor * vec4(vec3(diffuseIntensity + ambientIntensity), 1.0f) + specularColor * vec4(vec3(1.0f) * specularIntensity, 0.0f);
}
