#version 410 core // Minimal GL version support expected from the GPU
#define M_PI 3.1415926535897932384626433832795

// Shader inputs, linearly interpolated by default from the previous stage
in vec3 fPosition;
in vec3 fNormal;
in vec3 fTangent;
in vec3 fBinormal;
in vec2 fTexCoord;

uniform sampler2D diffuseTex, normalTex, specularTex;

// Shader output: the color response attached to this fragment
out vec4 colorResponse;

void main() {
    vec3 n = fNormal;

    //TODO: Make light as object
    vec3 lPosition = vec3(10.0f, 20.0f, 10.0f);
    lPosition = (vec4(lPosition, 1.0)).xyz;
    //    lPosition = (modelViewMat * vec4(lPosition, 1.0)).xyz;

    // Light incident vector
    vec3 wi = normalize(lPosition - fPosition);
    // Outgoing ray
    vec3 wo = normalize(-fPosition);
    // Half angle
    vec3 wh = normalize(wi + wo);

    float diffuseIntensity = clamp(dot(n, wi), 0.0f, 1.0f);

    vec4 diffuseColor = vec4(190.0f, 144.0f, 87.0f, 255.0f) / 255.0f;

    colorResponse = diffuseColor * vec4(vec3(diffuseIntensity), 1.0f);
}
