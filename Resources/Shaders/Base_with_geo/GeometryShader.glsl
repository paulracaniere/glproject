#version 410 core // Minimal GL version support expected from the GPU

#define NB_VERTICES 5

layout(triangles) in;
layout (line_strip, max_vertices=NB_VERTICES) out;

in vec3 gPosition[3];
in vec3 gNormal[3];
in vec3 gTangent[3];
in vec3 gBinormal[3];
in vec2 gTexCoord[3];

out vec3 fPosition;
out vec3 fNormal;
out vec3 fTangent;
out vec3 fBinormal;
out vec2 fTexCoord;

uniform mat4 projectionMat;

void main() {
    vec3 barycenter = (gPosition[0] + gPosition[1] + gPosition[2]) / 3.0f;
    vec3 faceNormal = (gNormal[0] + gNormal[1] + gNormal[2]) / 3.0f;

    gl_Position = projectionMat * vec4(barycenter, 1.0f);
    fPosition = barycenter;
    //FIXME
    fNormal = faceNormal;
    fTangent = gTangent[0];
    fBinormal = gBinormal[0];
    fTexCoord = gTexCoord[0];
    EmitVertex();

    vec3 pos = barycenter;
    vec3 acceleration = faceNormal;
    for (int i = 1; i < NB_VERTICES; i++) {
        pos += 0.1f * acceleration;
        gl_Position = projectionMat * vec4(pos, 1.0f);
        fPosition = barycenter;
        //FIXME
        fNormal = faceNormal;
        fTangent = gTangent[0];
        fBinormal = gBinormal[0];
        fTexCoord = gTexCoord[0];
        EmitVertex();

        acceleration += 0.8f * vec3(0.0f, -1.0f, 0.0f);
    }

    EndPrimitive();
}
