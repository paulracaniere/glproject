#version 410 core // Minimal GL version support expected from the GPU

layout(location=0) in vec3 vPosition;
layout(location=1) in vec3 vNormal;
layout(location=2) in vec3 vTangent;
layout(location=3) in vec3 vBinormal;
layout(location=4) in vec2 vTexCoord;

uniform mat4 projectionMat, modelViewMat, normalMat;

out vec3 gPosition;
out vec3 gNormal;
out vec3 gTangent;
out vec3 gBinormal;
out vec2 gTexCoord;

void main() {
    vec4 p = modelViewMat * vec4 (vPosition, 1.0f);
    gl_Position = projectionMat * p;// mandatory to fire rasterization properly
    gPosition = p.xyz;
    gNormal = normalize((normalMat * vec4 (vNormal, 1.0f)).xyz);
    gTangent = normalize((normalMat * vec4(vTangent, 1.0f)).xyz);
    gBinormal = normalize((normalMat * vec4(vBinormal, 1.0f)).xyz);
    gTexCoord = vTexCoord;
}