#version 410

in float fRealDepth;

//layout(location = 0) out float fragmentdepth;

out vec4 color;

void main() {
//    fragmentdepth = gl_FragCoord.z;
    color = vec4(vec3(- 5 / fRealDepth), 1.0f);
//    color = vec4(vec3(1.0f), 1.0f);
}
