#version 410
#define M_PI 3.14159265358979323846

//layout(isolines, point_mode) in;
layout(isolines) in;

patch in vec4 p_1, p2;
in float teProgression[];

uniform float tess_uniform;
uniform mat4 modelViewMat, normalMat;

out vec4 last_point;
out vec4 next_point;
out vec3 tangent;
out vec3 normal;
out vec3 bitangent;

void catmull_rom(in float t, out vec4 interpolated) {
    vec4 p0 = gl_in[0].gl_Position;
    vec4 p1 = gl_in[1].gl_Position;

    float b0 = (-1.f * t) + (2.f * t * t) + (-1.f * t * t * t);
    float b1 = (2.f) + (-5.f * t * t) + (3.f * t * t * t);
    float b2 = (t) + (4.f * t * t) + (-3.f * t * t * t);
    float b3 = (-1.f * t * t) + (t * t * t);
    interpolated = modelViewMat * (0.5f * (b0 * p_1 + b1 * p0 + b2 * p1 + b3 * p2));
}

void catmull_rom_tangent(in float t, out vec3 tan_dir) {
    vec4 p0 = gl_in[0].gl_Position;
    vec4 p1 = gl_in[1].gl_Position;

    float db0 = (-1.f) + (4.f * t) + (-3.f * t * t);
    float db1 = (-10.f * t) + (9.f * t * t);
    float db2 = (1.f) + (8.f * t) + (-9.f * t * t);
    float db3 = (-2.f * t) + (3.f * t * t);
    tan_dir = (modelViewMat * -vec4((0.5f * (db0 * p_1 + db1 * p0 + db2 * p1 + db3 * p2)).xyz, 0.0f)).xyz;
    tan_dir = normalize(tan_dir);
}

void catmull_rom_normal(in float t, in vec3 tan_dir, out vec3 nrm_dir) {
    vec4 p0 = gl_in[0].gl_Position;
    vec4 p1 = gl_in[1].gl_Position;

    float d2b0 = (4.f) + (-6.f * t);
    float d2b1 = (-10.f) + (18.f * t);
    float d2b2 = (8.f) + (-18.f * t);
    float d2b3 = (-2.f) + (6.f * t);
    nrm_dir = (modelViewMat * vec4((0.5f * (d2b0 * p_1 + d2b1 * p0 + d2b2 * p1 + d2b3 * p2)).xyz, 0.0f)).xyz;
    nrm_dir = nrm_dir - dot(nrm_dir, tan_dir) * tan_dir;
    nrm_dir = normalize(nrm_dir);
}

void main() {
    float u = gl_TessCoord.x;

    // Floating index of the ply to draw
    float ratio_ply = gl_TessCoord.y;

    vec4 central_pos;
    catmull_rom(u, central_pos);
    catmull_rom(u - 1.0f / tess_uniform, last_point);
    catmull_rom(u + 1.0f / tess_uniform, next_point);

    // Computing the tangent of the curve
    vec3 last_tangent, next_tangent;
    catmull_rom_tangent(u, tangent);
    catmull_rom_tangent(u - 1.0f / tess_uniform, last_tangent);
    catmull_rom_tangent(u + 1.0f / tess_uniform, next_tangent);

    // Generating a normal to the axis of the curve (any orthogonal to the tangent vector)
    vec3 last_normal, next_normal;
    catmull_rom_normal(u, tangent, normal);
    catmull_rom_normal(u - 1.0f / tess_uniform, last_tangent, last_normal);
    catmull_rom_normal(u + 1.0f / tess_uniform, next_tangent, next_normal);

    // Lastly, the bitangent
    vec3 last_bitangent, next_bitangent;
    bitangent = normalize(cross(tangent, normal));
    last_bitangent = normalize(cross(last_tangent, last_normal));
    next_bitangent = normalize(cross(next_tangent, next_normal));

    float progression, last_progression, next_progression;
    progression = mix(teProgression[0], teProgression[1], u);
    last_progression = mix(teProgression[0], teProgression[1], u - 1.0f / tess_uniform);
    next_progression = mix(teProgression[0], teProgression[1], u + 1.0f / tess_uniform);

    float radius_ply = 0.05f;
    float twist_ply = 3.0f;

    float theta = progression * twist_ply;
    float last_theta = last_progression * twist_ply;
    float next_theta = next_progression * twist_ply;

    float phase_ply = theta + ratio_ply * 2.0f * M_PI;
    float last_phase_ply = last_theta + ratio_ply * 2.0f * M_PI;
    float next_phase_ply = next_theta + ratio_ply * 2.0f * M_PI;
    central_pos.xyz += radius_ply * (cos(phase_ply) * normal + sin(phase_ply) * bitangent);
    last_point.xyz += radius_ply * (cos(last_phase_ply) * last_normal + sin(last_phase_ply) * last_bitangent);
    next_point.xyz += radius_ply * (cos(next_phase_ply) * next_normal + sin(next_phase_ply) * next_bitangent);

    gl_Position = central_pos;
}
