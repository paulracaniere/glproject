#version 410

layout(vertices = 2) out;

uniform mat4 projectionMat, modelViewMat, normalMat;
uniform float tess_uniform;
uniform int nb_plies;

in float tcProgression[];
out float teProgression[];

patch out vec4 p_1;
patch out vec4 p2;

void main() {
    gl_TessLevelOuter[0] = float(nb_plies);
    gl_TessLevelOuter[1] = floor(tess_uniform);

    if(gl_InvocationID == 0) {
        p_1 = gl_in[0].gl_Position;
        p2 = gl_in[3].gl_Position;

        gl_out[gl_InvocationID].gl_Position = gl_in[1].gl_Position;
        teProgression[gl_InvocationID] = tcProgression[1];
    }

    if(gl_InvocationID == 1) {
        gl_out[gl_InvocationID].gl_Position = gl_in[2].gl_Position;
        teProgression[gl_InvocationID] = tcProgression[2];
    }
}
