#version 410

layout(lines) in;
layout(triangle_strip, max_vertices = 4) out;

uniform mat4 projectionMat;
uniform float thickness;

in vec4 last_point[2];
in vec4 next_point[2];
in vec3 tangent[2];
in vec3 normal[2];
in vec3 bitangent[2];

out float fRealDepth;

void main() {
    vec3 color0 = vec3(1.0f, 0.0f, 0.0f);
    vec3 color1 = vec3(1.0f, 1.0f, 0.0f);

    vec2 normal01 = mat2(0.0f, -1.0f, 1.0f, 0.0f) *  normalize(next_point[0].xy - last_point[1].xy);
    vec2 normal_10 = mat2(0.0f, -1.0f, 1.0f, 0.0f) * normalize(last_point[1].xy - last_point[0].xy);
    vec2 normal12 = mat2(0.0f, -1.0f, 1.0f, 0.0f) *  normalize(next_point[1].xy - next_point[0].xy);

    vec2 dir0 = normalize(normal_10 + normal01);
    dir0 *= clamp(1.0f / dot(dir0, normal01), 0.0f, 1.5f);
    vec2 dir1 = normalize(normal01 + normal12);
    dir1 *= clamp(1.0f / dot(dir1, normal01), 0.0f, 1.5f);

    float size_of_gradient = 5.0f;
    float m_thickness = 20.0f * thickness;

    gl_Position = gl_in[0].gl_Position;
    gl_Position.xy += m_thickness * dir0;
    fRealDepth = gl_Position.z;
    gl_Position = projectionMat * gl_Position;
    EmitVertex();

    gl_Position = gl_in[1].gl_Position;
    gl_Position.xy += m_thickness * dir1;
    fRealDepth = gl_Position.z;
    gl_Position = projectionMat * gl_Position;
    EmitVertex();

    gl_Position = gl_in[0].gl_Position;
    gl_Position.xy -= m_thickness * dir0;
    fRealDepth = gl_Position.z;
    gl_Position = projectionMat * gl_Position;
    EmitVertex();

    gl_Position = gl_in[1].gl_Position;
    gl_Position.xy -= m_thickness * dir1;
    fRealDepth = gl_Position.z;
    gl_Position = projectionMat * gl_Position;
    EmitVertex();

    EndPrimitive();
}
