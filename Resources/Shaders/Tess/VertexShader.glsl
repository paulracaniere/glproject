#version 410 core // Minimal GL version support expected from the GPU

layout(location=0) in vec3 vPosition;

void main() {
    gl_Position =  vec4(vPosition, 1.0f);
}