#version 410 core // Minimal GL version support expected from the GPU

layout(vertices = 4) out;

uniform int tess_level;

void main() {
    int tess = tess_level;

    gl_TessLevelOuter[0] = tess;
    gl_TessLevelOuter[1] = tess;
    gl_TessLevelOuter[2] = tess;
    gl_TessLevelOuter[3] = tess;

    gl_TessLevelInner[0] = tess;
    gl_TessLevelInner[1] = tess;

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}
