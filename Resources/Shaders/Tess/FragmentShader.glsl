#version 410 core // Minimal GL version support expected from the GPU

// Shader output: the color response attached to this fragment
out vec4 colorResponse;

in vec3 fPosition, fNormal;

uniform mat4 modelViewMat;

void main() {
//        vec3 light_pos = (modelViewMat * vec4(vec3(20, 5, 0), 1.0f)).xyz;
    vec3 light_pos = (modelViewMat * vec4(vec3(0.0f, 20.0f, 20.0f), 1.0f)).xyz;

    float intensity = 0.1f;

    vec3 wo = normalize(-fPosition);
    vec3 wi = normalize(light_pos - fPosition);
    vec3 wh = normalize(wi + wo);

    vec3 diffuse = intensity * abs(dot(wi, fNormal)) * vec3(0.0f, 0.0f, 1.0f);

    vec3 spec = intensity * pow(dot(wh, wi), 256) * vec3(0.8f, 1.0f, 0.8f);
//    vec3 spec = vec3(0.0f);

    colorResponse = vec4(diffuse + spec, 1.0f);
//    colorResponse = vec4((fNormal + 1.0f) / 2.0f, 1.0f);
}
