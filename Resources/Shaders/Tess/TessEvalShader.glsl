#version 410 core // Minimal GL version support expected from the GPU

layout(quads, equal_spacing, ccw) in;

uniform mat4 projectionMat, modelViewMat, normalMat;

out vec3 fPosition, fNormal;

uniform float anim_time;

void main() {

    // Interpolated points at the two opposite edges of the quad
    vec4 p1 = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);
    vec4 p2 = mix(gl_in[3].gl_Position, gl_in[2].gl_Position, gl_TessCoord.x);

    // Interpolated point using the interpolated points of the quad's edges
    vec4 p = mix(p1, p2, gl_TessCoord.y);

    // Initialization
    fNormal = vec3(0.0f);
    float sp_freq, ti_freq;
    vec3 gx, gz;
    vec3 dir;
    float ampl;

    // Deformation 1
    sp_freq = 0.05f;
    ti_freq = 0.01f;
    dir = normalize(vec3(1.0f, 0.0f, 1.0f));
    ampl = 5.0f;

    p.y += ampl * cos(anim_time * ti_freq - dot(p.xyz, dir) * sp_freq);
    gx = vec3(1.0, sp_freq * ampl * dot(dir, vec3(1.0f, 0.0f, 0.0f)) * sin(anim_time * ti_freq - dot(p.xyz, dir) * sp_freq), 0.0);
    gz = vec3(0.0, sp_freq * ampl * dot(dir, vec3(0.0f, 0.0f, 1.0f)) * sin(anim_time * ti_freq - dot(p.xyz, dir) * sp_freq), 1.0);
    fNormal += (normalMat * vec4(normalize(cross(gz, gx)), 0.0f)).xyz;

    // Deformation 2
    sp_freq = 0.2f;
    ti_freq = 0.02f;
    dir = normalize(vec3(-1.0f, 0.f, 0.5f));
    ampl = 3.0f;

    p.y += ampl * cos(anim_time * ti_freq - dot(p.xyz, dir) * sp_freq);
    gx = vec3(1.0, sp_freq * ampl * dot(dir, vec3(1.0f, 0.0f, 0.0f)) * sin(anim_time * ti_freq - dot(p.xyz, dir) * sp_freq), 0.0);
    gz = vec3(0.0, sp_freq * ampl * dot(dir, vec3(0.0f, 0.0f, 1.0f)) * sin(anim_time * ti_freq - dot(p.xyz, dir) * sp_freq), 1.0);
    fNormal += (normalMat * vec4(normalize(cross(gz, gx)), 0.0f)).xyz;

    // Deformation 2
    sp_freq = 1.0f;
    ti_freq = 0.01f;
    dir = normalize(vec3(0.3f, 0.f, -0.5f));
    ampl = 0.3f;

    p.y += ampl * cos(anim_time * ti_freq - dot(p.xyz, dir) * sp_freq);
    gx = vec3(1.0, sp_freq * ampl * dot(dir, vec3(1.0f, 0.0f, 0.0f)) * sin(anim_time * ti_freq - dot(p.xyz, dir) * sp_freq), 0.0);
    gz = vec3(0.0, sp_freq * ampl * dot(dir, vec3(0.0f, 0.0f, 1.0f)) * sin(anim_time * ti_freq - dot(p.xyz, dir) * sp_freq), 1.0);
    fNormal += (normalMat * vec4(normalize(cross(gz, gx)), 0.0f)).xyz;

    fPosition = (modelViewMat * p).xyz;
    gl_Position = projectionMat * modelViewMat * p;
}
