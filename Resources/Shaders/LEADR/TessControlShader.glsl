#version 410

layout(vertices = 4) out;

in vec3 tcNormal[];
in vec3 tcTangent[];
in vec3 tcBinormal[];
in vec2 tcUV[];

uniform float tess_level;

out vec3 teNormal[];
out vec3 teTangent[];
out vec3 teBinormal[];
out vec2 teUV[];

void main() {
    int tess = int(tess_level);

    gl_TessLevelOuter[0] = tess;
    gl_TessLevelOuter[1] = tess;
    gl_TessLevelOuter[2] = tess;
    gl_TessLevelOuter[3] = tess;

    gl_TessLevelInner[0] = tess;
    gl_TessLevelInner[1] = tess;

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

    teNormal[gl_InvocationID] = tcNormal[gl_InvocationID];
    teTangent[gl_InvocationID] = tcTangent[gl_InvocationID];
    teBinormal[gl_InvocationID] = tcBinormal[gl_InvocationID];
    teUV[gl_InvocationID] = tcUV[gl_InvocationID];
}
