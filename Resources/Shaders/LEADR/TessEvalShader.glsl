#version 410

layout(quads, equal_spacing, ccw) in;

in vec3 teNormal[];
in vec3 teTangent[];
in vec3 teBinormal[];
in vec2 teUV[];

uniform mat4 projectionMat, modelViewMat, normalMat;
uniform sampler2D heightTex;
uniform float LEADR_scale_level, LEADR_repeat;

out vec3 fPos;
out vec3 fNormal;
out vec3 fTangent;
out vec3 fBinormal;
out vec2 fUV;

void main() {

    // Interpolated point using the interpolated points of the quad's edges
    vec4 p = mix(
        mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x),
        mix(gl_in[3].gl_Position, gl_in[2].gl_Position, gl_TessCoord.x),
        gl_TessCoord.y);

    // Same for other data
    fNormal = normalize(mix(
        mix(teNormal[0], teNormal[1], gl_TessCoord.x),
        mix(teNormal[3], teNormal[2], gl_TessCoord.x),
        gl_TessCoord.y));

    fTangent = normalize(mix(
        mix(teTangent[0], teTangent[1], gl_TessCoord.x),
        mix(teTangent[3], teTangent[2], gl_TessCoord.x),
        gl_TessCoord.y));

    fBinormal = normalize(mix(
        mix(teBinormal[0], teBinormal[1], gl_TessCoord.x),
        mix(teBinormal[3], teBinormal[2], gl_TessCoord.x),
        gl_TessCoord.y));

    fUV = LEADR_repeat * mix(
        mix(teUV[0], teUV[1], gl_TessCoord.x),
        mix(teUV[3], teUV[2], gl_TessCoord.x),
        gl_TessCoord.y);

    // Height map
    float height = LEADR_scale_level * (texture(heightTex, fUV).r - 0.5f) * 2.0f; //FIXME: Why *2.0 ?

    // Displacement
    p += vec4(fNormal, 0.0f) * height;

    fPos = (modelViewMat * p).xyz;

    gl_Position = projectionMat * modelViewMat * p;
}
