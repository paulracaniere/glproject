#version 410
#define M_PI 3.1415926535897932384626433832795
#define MAX_SIZE_ARRAY 10

in vec3 fPos;
in vec3 fNormal;
in vec3 fTangent;
in vec3 fBinormal;
in vec2 fUV;

uniform mat4 projectionMat, modelViewMat, normalMat;
uniform sampler2D heightTex, LEADRTex0, LEADRTex1, albedoTex;
uniform samplerCube cubemapTex;
uniform float LEADR_scale_level, LEADR_stretch, LEADR_repeat;
uniform int LEADR_example;

// Sampling
uniform int samplingSize;
uniform float samplingWeights[MAX_SIZE_ARRAY], samplingPosition[MAX_SIZE_ARRAY];
float lambda = 1.8f;

out vec4 colorResponse;

float height;
vec4 leadr0;
float leadr1;
float Ex, Ey, sigmax, sigmay, cxy;

void load_LEADR() {
    height = LEADR_scale_level * (texture(heightTex, fUV).r - 0.5f);
    leadr0 = texture(LEADRTex0, fUV);
    leadr0.rg = LEADR_scale_level * leadr0.rg;
    leadr0.ba = LEADR_scale_level * LEADR_scale_level * leadr0.ba;
    leadr1 = LEADR_scale_level * LEADR_scale_level * texture(LEADRTex1, fUV).r;
}

// TODO: Account for texture shearing
void load_moments() {
    Ex = leadr0.x;
    Ey = leadr0.y;
    sigmax = sqrt(max(leadr0.z - pow(leadr0.x, 2), 0.0f)) + 0.001f;
    sigmay = sqrt(max(leadr0.w - pow(leadr0.y, 2), 0.0f)) + 0.001f;
    cxy = leadr1 - leadr0.x * leadr0.y;

    // Stretching
    Ex /= LEADR_stretch;
    sigmax /= LEADR_stretch;
    cxy /= LEADR_stretch;

    // Texture repeating = zooming out
    Ex *= LEADR_repeat;
    Ey *= LEADR_repeat;
    sigmax *= LEADR_repeat;
    sigmay *= LEADR_repeat;
    cxy *= LEADR_repeat * LEADR_repeat;
}

// Walter (2007) approximation for Lambda function from Smith (1967)
float masking_shadowing(vec3 omega) {
    omega = normalize(omega);
    float theta = acos(omega.z);
    float phi;
    if (sin(theta) < 0.01f) phi = M_PI / 4.0f;
    else phi = acos(omega.x / sin(theta));
    float mu = cos(phi) * Ex + sin(phi) * Ey;
    float sigma = sqrt(pow(cos(phi) * sigmax, 2) + pow(sin(phi) * sigmay, 2) + 2.0f * cos(phi) * sin(phi) * cxy);
    float nu = (1.0f / tan(theta) - mu) / (sigma * sqrt(2.0f));
    if (nu < 1.6f) return (1.0f - 1.259f * nu + 0.396f * nu * nu ) / (3.535f * nu + 2.181f * nu * nu);
    else return 0.0f;
}

// Schlick approximation
float fresnel(vec3 wh, vec3 wi) {
    float n2 = 3.47f; // Silicium
    // Standard reflection ratio for Fresnel term
    float R0 = pow((n2 - 1.0f) / (n2 + 1.0f), 2);
    return R0 + (1 - R0) * pow(1 - dot(wh, wi), 5);
}

float probability(vec2 slope) {
    mat2 covariance = mat2(pow(sigmax, 2), cxy, cxy, pow(sigmay, 2));
    float det = determinant(covariance);
    if (det <= 0.0f) {
        if (slope == vec2(Ex, Ey)) return 1.0f;
        else return 0.0f;
    }
    return exp(-0.5f * dot(slope - vec2(Ex, Ey), inverse(covariance) * (slope - vec2(Ex, Ey)))) /
        (2.0f * M_PI * sqrt(determinant(covariance)));
}

// wn must be in tangent space
float distribution(vec3 wn) {
    vec2 slope = vec2(-wn.x / wn.z, -wn.y / wn.z);
    return probability(slope) / pow(wn.z, 4);
}

vec3 point_light_for_specular(vec3 L, vec3 wi, vec3 wo) {
    vec3 mesonormal = normalize(vec3(-Ex, -Ey, 1.0f));
    vec3 wh = normalize(wo + wi);
//    if (dot(mesonormal, wo) < 0.0f) return vec3(1.0f, 0.0f, 0.0f);
    return L * distribution(wh) *
        mesonormal.z /
        dot(mesonormal, wo) *
        fresnel(wh, wi) /
        4.0f /
//        (1.0f + masking_shadowing(wo) + masking_shadowing(wi));
        (1.0f + masking_shadowing(wo));
}

//TODO: Precompute SH of envmap on CPU
void point_light_irradiance_map_for_diffuse(
    vec3 L, vec3 wi, vec3 wo,
    out vec3 L00,
    out vec3 L11, out vec3 L10, out vec3 L1_1,
    out vec3 L21, out vec3 L2_1, out vec3 L2_2, out vec3 L20, out vec3 L22) {
//    vec3 P = L / (1.0f + masking_shadowing(wo) + masking_shadowing(wi));
    vec3 P = L;
    L00 = 0.282095f * P;
    L11 = 0.488603f * wi.x * P;
    L10 = 0.488603f * wi.y * P;
    L1_1 = 0.488603f * wi.z * P;
    L21 = 1.092548f * (wi.x * wi.z) * P;
    L2_1 = 1.092548f * (wi.y * wi.z) * P;
    L2_2 = 1.092548f * (wi.x * wi.y) * P;
    L20 = 0.315392f * (3.0f * wi.z * wi.z - 1.0f) * P;
    L22 = 0.549274f * (wi.x * wi.x - wi.y * wi.y) * P;
}

vec3 sum_irradiance(vec3 L, vec3 wi, vec3 wo, vec3 wn) {
    vec3 L00, L11, L10, L1_1, L21, L2_1, L2_2, L20, L22;
    point_light_irradiance_map_for_diffuse(L, wi, wo, L00, L11, L10, L1_1, L21, L2_1, L2_2, L20, L22);
    vec3 E = vec3(0.0f);
    E += 0.429043f * L22 * (wn.x * wn.x - wn.y * wn.y);
    E += 0.743125f * L20 * wn.z * wn.z;
    E += 0.886227f * L00;
    E -= 0.247708 * L20;
    E += 0.858086f * (L2_2 * wn.x * wn.y + L21 * wn.x * wn.z + L2_1 * wn.y * wn.z);
    E += 1.023328f * (L11 * wn.x + L1_1 * wn.y + L10 * wn.z);
    return E;
}

vec3 point_light_for_diffuse(vec3 L, vec3 wi, vec3 wo) {
    float rxy;
    if (sigmax == 0 || sigmay == 0) rxy = 0.0f;
    else rxy = cxy / sigmax / sigmay;
    int N = samplingSize;
    int memsize = (N + 1) / 2;
    vec3 I = vec3(0.0f);

    // Lambda function without shadowing
    float S = 0.0f;

    int k = 2, j = 4;
    for (int j = 0; j < N; j++) {
        for (int k = 0; k < N; k++) {
            float pj = (j < memsize ? -1.0f : 1.0f) * samplingPosition[abs(j - memsize + 1 - ((N % 2 == 0 && j >= memsize) ? 1 : 0))];
            float pk = (k < memsize ? -1.0f : 1.0f) * samplingPosition[abs(k - memsize + 1 - ((N % 2 == 0 && k >= memsize) ? 1 : 0))];

            float xn = pj * sigmax + Ex;
            float yn = (rxy * pj + sqrt(1.0f - rxy * rxy) * pk) * sigmay + Ey;

            float Wn = samplingWeights[abs(j - memsize + 1 - ((N % 2 == 0 && j >= memsize) ? 1 : 0))] *
                       samplingWeights[abs(k - memsize + 1 - ((N % 2 == 0 && k >= memsize) ? 1 : 0))];

            vec3 wn = normalize(vec3(-xn, -yn, 1.0f));

//            I += Wn * max(dot(wn, wo), 0.0f) / wn.z * sum_irradiance(L, wi, wo, wn);
            I += Wn * max(dot(wn, wo), 0.0f) / wn.z * vec3(max(dot(wn, wi), 0.0f)) * L;

            S += Wn * max(dot(wn, wo), 0.0f) / wn.z;
        }
    }
//    return I / dot(vec3(-Ex, -Ey, 1.0f), wo) / M_PI;
    return I / S / M_PI;
//    return I;
}

vec3 environment_lighting_for_specular(vec3 wo) {
    float rxy;
    if (sigmax == 0 || sigmay == 0) rxy = 0.0f;
    else rxy = cxy / sigmax / sigmay;
    int N = samplingSize;
    float A = pow(2.0f * lambda * max(sigmax, sigmax) / N, 2);
    int memsize = (N + 1) / 2;

    vec3 I = vec3(0.0f);
    float S = 0.0f;

    for (int j = 0; j < N; j++) {
        for (int k = 0; k < N; k++) {
            float pj = (j < memsize ? -1.0f : 1.0f) * samplingPosition[abs(j - memsize + 1 - ((N % 2 == 0 && j >= memsize) ? 1 : 0))];
            float pk = (k < memsize ? -1.0f : 1.0f) * samplingPosition[abs(k - memsize + 1 - ((N % 2 == 0 && k >= memsize) ? 1 : 0))];

            float xn = pj * sigmax + Ex;
            float yn = (rxy * pj + sqrt(1.0f - rxy * rxy) * pk) * sigmay + Ey;

            float Wn = samplingWeights[abs(j - memsize + 1 - ((N % 2 == 0 && j >= memsize) ? 1 : 0))] *
                       samplingWeights[abs(k - memsize + 1 - ((N % 2 == 0 && k >= memsize) ? 1 : 0))];

            vec3 wn = normalize(vec3(-xn, -yn, 1.0f));
            vec3 wi = reflect(-wo, wn);
            vec3 wh = normalize(wi + wo);

            // Compute solid angle for LoD of envmap
            float J = 4.0f * abs(dot(wn, wo)) * pow(abs(wn.z), 3);
            float alpha = J * A;

            // LoD function for cubemap
            float lod = log2(sqrt(alpha / 4.0f / M_PI) * textureSize(cubemapTex, 0).x);

//            I += textureLod(cubemapTex, mat3(fTangent, fBinormal, fNormal) * wi, lod).rgb * (
//                Wn * fresnel(wh, wi) * max(dot(wn, wo), 0.0f) / wn.z /
//                (1.0f + masking_shadowing(wo) + masking_shadowing(wi)));

            I += textureLod(cubemapTex, mat3(fTangent, fBinormal, fNormal) * wi, lod).rgb *
                Wn *
                fresnel(wn, wi) *
                max(dot(wn, wo), 0.0f) /
                wn.z;
            S += Wn * max(dot(wn, wo), 0.0f) / wn.z;
        }
    }
//    return I / dot(vec3(-Ex, -Ey, 1.0f), wo);
    return I / S;
}

void main() {
    load_LEADR();
    load_moments();

    mat3 modelToTangent = transpose(mat3(fTangent, fBinormal, fNormal));

    // Light and surface orientation parameters
    vec3 wo = modelToTangent * (inverse(modelViewMat) * vec4(normalize(-fPos), 0.0f)).xyz;

//    // Directional light
//    vec3 wi = modelToTangent * normalize(vec3(0.0, 0.0, 1.0f));
    // Point light
    vec3 wi = modelToTangent * normalize(vec3(1.0f, 0.0f, 1.0f) - (inverse(modelViewMat) * vec4(fPos, 1.0f)).xyz);

    vec3 L_spec = 0.2f * vec3(1.0f);
//    vec3 L_diff = vec3(0.2f, 0.7f, 0.6f);
    vec3 L_diff = 2.0f * texture(albedoTex, fUV).rgb;

    vec3 mesonormal = normalize(vec3(-Ex, -Ey, 1.0f));
    vec3 unfiltered_mesonormal = normalize(vec3(
        -textureLod(LEADRTex0, fUV, 0).r * LEADR_scale_level / LEADR_stretch * LEADR_repeat,
        -textureLod(LEADRTex0, fUV, 0).g * LEADR_scale_level * LEADR_repeat,
        1.0f));
    vec3 wh = normalize(wi + wo);

    switch (LEADR_example) {
        case (0):
            colorResponse.xyz = point_light_for_specular(L_spec, wi, wo);
            break;
        case (1):
            colorResponse.xyz = point_light_for_diffuse(L_diff, wi, wo);
            break;
        case (2):
            colorResponse.xyz = environment_lighting_for_specular(wo);
            break;
        case (3):
            colorResponse.xyz = point_light_for_diffuse(L_diff, wi, wo) + point_light_for_specular(L_spec, wi, wo);
            break;
        case (4):
            colorResponse.xyz = (mesonormal + 1.0f) / 2.0f;
            break;
        case (5):
            colorResponse.xyz = vec3(pow(max(0.0f, dot(wh, mesonormal)), 1024));
            break;
        case (6):
            colorResponse.xyz = vec3(pow(max(0.0f, dot(wh, unfiltered_mesonormal)), 1024));
            break;
    }
}
