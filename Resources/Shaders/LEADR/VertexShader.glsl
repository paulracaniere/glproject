#version 410

layout(location=0) in vec3 vPosition;
layout(location=1) in vec3 vNormal;
layout(location=2) in vec3 vTangent;
layout(location=3) in vec3 vBinormal;
layout(location=4) in vec2 vUV;

uniform float LEADR_stretch;

out vec3 tcNormal;
out vec3 tcTangent;
out vec3 tcBinormal;
out vec2 tcUV;

void main() {
    gl_Position =  vec4(vPosition, 1.0f);
    gl_Position.x *= LEADR_stretch;
    tcNormal = vNormal;
    tcTangent = vTangent;
    tcBinormal = vBinormal;
    tcUV = vUV;
}
