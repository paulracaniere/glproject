#version 410

layout(vertices = 2) out;

uniform mat4 projectionMat, modelViewMat, normalMat;
uniform float tess_uniform, thickness;
uniform int nb_plies, nb_fibers_per_ply;

in float tcProgression[];
out float teProgression[];

patch out vec4 p_1, p2;
patch out float lambda2;

bool out_of_frustum(in vec4 point) {
    vec4 projected = projectionMat * modelViewMat * point;
    bool res = false;
    if (abs(projected.x / projected.w) > 1.0f) res = true;
    if (abs(projected.y / projected.w) > 1.0f) res = true;
    if (abs(projected.z / projected.w) > 1.0f) res = true;
    return res;
}

//FIXME
float ratio_after_projection(in vec4 point) {
    vec4 projected = modelViewMat * point;
    return - 20.0f / projected.z;
}

void main() {

    // Discard out of frustum
    float visible = 1.0f;
    if (out_of_frustum(gl_in[1].gl_Position) && out_of_frustum(gl_in[2].gl_Position)) visible = 0.0f;

    // LoD

    // Screen-space width of a fiber placed at the center of the yarn curve
    float w_fiber = thickness * ratio_after_projection(gl_in[1].gl_Position);
    // Width of a ply
    float w_ply = w_fiber * 20.0f; // FIXME: Use actual value
    // User-defined threshold (typically smaller than a pixel)
    float w_lod = 0.005f; // FIXME: Fit to 1 pixel of screen or more if slow hardware
    // LoD scale factor lambda^2
//    float lambda2;
    if (w_lod < w_fiber) lambda2 = 1.0f;
    else if (w_ply < w_lod) lambda2 = 0.0f;
    else lambda2 = pow(w_fiber / w_lod * (w_ply - w_lod) / (w_ply - w_fiber), 2);

    if(gl_InvocationID == 0) {
        p_1 = gl_in[0].gl_Position;
        p2 = gl_in[3].gl_Position;

        gl_out[gl_InvocationID].gl_Position = gl_in[1].gl_Position;
        teProgression[gl_InvocationID] = tcProgression[1];

        gl_TessLevelOuter[0] = float(nb_fibers_per_ply * nb_plies) * visible;
//        gl_TessLevelOuter[1] = max(1.0f, floor(tess_uniform));
        gl_TessLevelOuter[1] = max(1.0f, floor(lambda2 * tess_uniform));
    }

    if(gl_InvocationID == 1) {
        gl_out[gl_InvocationID].gl_Position = gl_in[2].gl_Position;
        teProgression[gl_InvocationID] = tcProgression[2];
    }
}
