#version 410
#define M_PI 3.14159265358979323846

layout(isolines) in;

patch in vec4 p_1, p2;

in float teProgression[];
patch in float lambda2;

uniform float tess_uniform;
uniform int nb_plies, nb_fibers_per_ply;
uniform mat4 modelViewMat, normalMat;
uniform sampler1D radiiAnglesTex;

out vec4 last_point;
out vec4 next_point;
out vec3 tangent;
out vec3 normal;
out vec3 bitangent;

out float gProgression;
out int gIndex;
out vec3 gColor;
out uint isCentral;

void catmull_rom(in float t, out vec4 interpolated) {
    vec4 p0 = gl_in[0].gl_Position;
    vec4 p1 = gl_in[1].gl_Position;

    float b0 = (-1.f * t) + (2.f * t * t) + (-1.f * t * t * t);
    float b1 = (2.f) + (-5.f * t * t) + (3.f * t * t * t);
    float b2 = (t) + (4.f * t * t) + (-3.f * t * t * t);
    float b3 = (-1.f * t * t) + (t * t * t);
    interpolated = modelViewMat * (0.5f * (b0 * p_1 + b1 * p0 + b2 * p1 + b3 * p2));
}

void catmull_rom_tangent(in float t, out vec3 tan_dir) {
    vec4 p0 = gl_in[0].gl_Position;
    vec4 p1 = gl_in[1].gl_Position;

    float db0 = (-1.f) + (4.f * t) + (-3.f * t * t);
    float db1 = (-10.f * t) + (9.f * t * t);
    float db2 = (1.f) + (8.f * t) + (-9.f * t * t);
    float db3 = (-2.f * t) + (3.f * t * t);
    tan_dir = (modelViewMat * -vec4((0.5f * (db0 * p_1 + db1 * p0 + db2 * p1 + db3 * p2)).xyz, 0.0f)).xyz;
    tan_dir = normalize(tan_dir);
}

void catmull_rom_normal(in float t, in vec3 tan_dir, out vec3 nrm_dir) {
    vec4 p0 = gl_in[0].gl_Position;
    vec4 p1 = gl_in[1].gl_Position;

    float d2b0 = (4.f) + (-6.f * t);
    float d2b1 = (-10.f) + (18.f * t);
    float d2b2 = (8.f) + (-18.f * t);
    float d2b3 = (-2.f) + (6.f * t);
    nrm_dir = (modelViewMat * vec4((0.5f * (d2b0 * p_1 + d2b1 * p0 + d2b2 * p1 + d2b3 * p2)).xyz, 0.0f)).xyz;
    nrm_dir = nrm_dir - dot(nrm_dir, tan_dir) * tan_dir;
    nrm_dir = normalize(nrm_dir);
}

void main() {
    float u = gl_TessCoord.x;
    float longitudinal_tesselation_used = gl_TessLevelOuter[1]; //FIXME: Does not work on MacOS

    int index = int(gl_TessCoord.y * gl_TessLevelOuter[0] + 0.1f);
    // Floating index of the ply to draw
    float ratio_ply = floor(index / nb_fibers_per_ply) / nb_plies;
    int index_fiber = index % nb_fibers_per_ply;

    vec4 central_pos;
    catmull_rom(u, central_pos);
    catmull_rom(u - 1.0f / longitudinal_tesselation_used, last_point);
    catmull_rom(u + 1.0f / longitudinal_tesselation_used, next_point);

    // Computing the tangent of the curve
    vec3 last_tangent, next_tangent;
//    catmull_rom_tangent(u, tangent);
//    catmull_rom_tangent(u - 1.0f / longitudinal_tesselation_used, last_tangent);
//    catmull_rom_tangent(u + 1.0f / longitudinal_tesselation_used, next_tangent);
    tangent = next_tangent = last_tangent = (modelViewMat * vec4(vec3(1.0f, 0.0f, 0.0f), 0.0f)).xyz;

    // Generating a normal to the axis of the curve (any orthogonal to the tangent vector)
    vec3 last_normal, next_normal;
//    catmull_rom_normal(u, tangent, normal);
//    catmull_rom_normal(u - 1.0f / longitudinal_tesselation_used, last_tangent, last_normal);
//    catmull_rom_normal(u + 1.0f / longitudinal_tesselation_used, next_tangent, next_normal);
    normal = next_normal = last_normal = (modelViewMat * vec4(vec3(0.0f, 1.0f, 0.0f), 0.0f)).xyz;

    // Lastly, the bitangent
    vec3 last_bitangent, next_bitangent;
    bitangent = normalize(cross(tangent, normal));
    last_bitangent = normalize(cross(last_tangent, last_normal));
    next_bitangent = normalize(cross(next_tangent, next_normal));

    float progression, last_progression, next_progression;
    progression = mix(teProgression[0], teProgression[1], u);
    last_progression = mix(teProgression[0], teProgression[1], u - 1.0f / longitudinal_tesselation_used);
    next_progression = mix(teProgression[0], teProgression[1], u + 1.0f / longitudinal_tesselation_used);

    float radius_ply = 0.05f;
//    float radius_ply = 0.0f;
    float twist_ply = 1.0f;
//    float twist_ply = 0.0f;

    float theta = progression * twist_ply;
    float last_theta = last_progression * twist_ply;
    float next_theta = next_progression * twist_ply;

    float phase_ply = theta + ratio_ply * 2.0f * M_PI;
    float last_phase_ply = last_theta + ratio_ply * 2.0f * M_PI;
    float next_phase_ply = next_theta + ratio_ply * 2.0f * M_PI;
    central_pos.xyz += radius_ply * (cos(phase_ply) * normal + sin(phase_ply) * bitangent);
    last_point.xyz += radius_ply * (cos(last_phase_ply) * last_normal + sin(last_phase_ply) * last_bitangent);
    next_point.xyz += radius_ply * (cos(next_phase_ply) * next_normal + sin(next_phase_ply) * next_bitangent);

    if (index_fiber > 0) {
        float fiber_twist = 1.5f; // Relative to ply twist

        vec3 N_ply = cos(phase_ply) * normal + sin(phase_ply) * bitangent;
        vec3 last_N_ply = cos(last_phase_ply) * last_normal + sin(last_phase_ply) * last_bitangent;
        vec3 next_N_ply = cos(next_phase_ply) * next_normal + sin(next_phase_ply) * next_bitangent;
        vec3 B_ply = cross(tangent, N_ply);
        vec3 last_B_ply = cross(last_tangent, last_N_ply);
        vec3 next_B_ply = cross(next_tangent, next_N_ply);

        float theta_i = texelFetch(radiiAnglesTex, index_fiber - 1, 0).g;
        float R = radius_ply * texelFetch(radiiAnglesTex, index_fiber - 1, 0).r;
//        float R = 0.05f * texelFetch(radiiAnglesTex, index_fiber - 1, 0).r * 1.5f;
        float R_max = 1.1f;
        float R_loop_max = 2.0f;
        float R_min = 0.9f;
        float s = 2.0f;
        if (index_fiber % 10 == 0) R_max = R_loop_max;
        R = R / 2.0f * (R_max + R_min + (R_max - R_min) * cos(theta_i + progression * M_PI * s));

        central_pos.xyz += 0.5f * R * (cos(fiber_twist * theta + theta_i) * N_ply + sin(fiber_twist * theta + theta_i) * B_ply * 1.8f);
        last_point.xyz += 0.5f * R * (cos(fiber_twist * last_theta + theta_i) * last_N_ply + sin(fiber_twist * last_theta + theta_i) * last_B_ply * 1.8f);
        next_point.xyz += 0.5f * R * (cos(fiber_twist * next_theta + theta_i) * next_N_ply + sin(fiber_twist * next_theta + theta_i) * next_B_ply * 1.8f);
//        central_pos.xyz += 0.5f * R * (cos(progression * M_PI + theta_i) * N_ply + sin(progression * M_PI + theta_i) * B_ply);
//        last_point.xyz += 0.5f * R * (cos(last_progression * M_PI + theta_i) * last_N_ply + sin(last_progression * M_PI + theta_i) * last_B_ply);
//        next_point.xyz += 0.5f * R * (cos(next_progression * M_PI + theta_i) * next_N_ply + sin(next_progression * M_PI + theta_i) * next_B_ply);

        switch (index / nb_fibers_per_ply) {
            case(0):
            gColor = vec3(1.0f, 1.0f, 0.2f);
            break;
            case(1):
            gColor = vec3(0.2f, 1.0f, 0.2f);
            break;
            case(2):
            gColor = vec3(0.2f, 0.2f, 1.0f);
            break;
            default:
            gColor = vec3(0.0f);
        }

//        gColor = vec3(texelFetch(radiiAnglesTex, index_fiber - 1, 0).r / 2.0f);

//        gColor = vec3(1.0f);

//        switch (index_fiber % 3) {
//            case(0):
//            gColor = vec3(1.0f, 1.0f, 0.2f);
//            break;
//            case(1):
//            gColor = vec3(0.2f, 1.0f, 0.2f);
//            break;
//            case(2):
//            gColor = vec3(0.2f, 0.2f, 1.0f);
//            break;
//            default:
//            gColor = vec3(0.0f);
//        }

        isCentral = 0;
    } else {
        switch (index / nb_fibers_per_ply) {
            case(0):
                gColor = vec3(1.0f, 1.0f, 0.2f) * 0.5f;
                break;
            case(1):
                gColor = vec3(0.2f, 1.0f, 0.2f) * 0.5f;
                break;
            case(2):
                gColor = vec3(0.2f, 0.2f, 1.0f) * 0.5f;
                break;
            default:
                gColor = vec3(0.0f);
        }

        isCentral = 1;
    }

    // To visualize LoD
//    gColor = vec3(1.0f, 0.0f, 0.0f) * lambda2 + (1.0f - lambda2) * vec3(0.0, 1.0f, 1.0f);

    gIndex = index_fiber;
    gProgression = progression;
    gl_Position = central_pos;
}
