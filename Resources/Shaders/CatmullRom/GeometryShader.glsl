#version 410

layout(lines) in;
layout(triangle_strip, max_vertices = 4) out;

uniform float thickness;
uniform int display_mode;
uniform mat4 projectionMat, depthModelViewMat, modelViewMat, depthProjectionMat;

in vec4 last_point[2];
in vec4 next_point[2];
in vec3 tangent[2];
in vec3 normal[2];
in vec3 bitangent[2];

in float gProgression[2];
in int gIndex[2];
in vec3 gColor[2];
in uint isCentral[2];

out vec3 fColor, fTangent, fNormal, fPosition;
out float fHorizontal, fVertical;

void main() {
    vec2 normal01 = mat2(0.0f, -1.0f, 1.0f, 0.0f) *  normalize(next_point[0].xy - last_point[1].xy);
    vec2 normal_10 = mat2(0.0f, -1.0f, 1.0f, 0.0f) * normalize(last_point[1].xy - last_point[0].xy);
    vec2 normal12 = mat2(0.0f, -1.0f, 1.0f, 0.0f) *  normalize(next_point[1].xy - next_point[0].xy);

    vec2 dir0 = normalize(normal_10 + normal01);
    dir0 *= clamp(1.0f / dot(dir0, normal01), 0.0f, 1.5f);
    vec2 dir1 = normalize(normal01 + normal12);
    dir1 *= clamp(1.0f / dot(dir1, normal01), 0.0f, 1.5f);

    // TODO: Make central adaptative with number of fibers displayed
    if (isCentral[0] == 1) {
        dir0 *= 15.0f;
        dir1 *= 15.0f;
//        dir0 *= 0.0f;
//        dir1 *= 0.0f;
    }

    float size_of_gradient = 5.0f;

    mat4 invModelViewMat = inverse(modelViewMat);

    vec3 discrete_tangent = normalize(
        (invModelViewMat * gl_in[1].gl_Position).xyz -
        (invModelViewMat * gl_in[0].gl_Position).xyz);
    fTangent = discrete_tangent;
    fNormal = cross((invModelViewMat * vec4(vec3(0.0f, 0.0f, 1.0f), 0.0f)).xyz, fTangent);

    gl_Position = gl_in[0].gl_Position;
    gl_Position.xy += thickness * dir0;
    fPosition = (invModelViewMat * gl_Position).xyz;
    gl_Position = projectionMat * gl_Position;
    fColor = gColor[0];
    fHorizontal = 0.0f;
    fVertical = 1.0f;
    EmitVertex();

    gl_Position = gl_in[1].gl_Position;
    gl_Position.xy += thickness * dir1;
    fPosition = (invModelViewMat * gl_Position).xyz;
    gl_Position = projectionMat * gl_Position;
    fColor = gColor[1];
    fHorizontal = 1.0f;
    fVertical = 1.0f;
    EmitVertex();

    gl_Position = gl_in[0].gl_Position;
    gl_Position.xy -= thickness * dir0;
    fPosition = (invModelViewMat * gl_Position).xyz;
    gl_Position = projectionMat * gl_Position;
    fColor = gColor[0];
    fHorizontal = 0.0f;
    fVertical = 0.0f;
    EmitVertex();

    gl_Position = gl_in[1].gl_Position;
    gl_Position.xy -= thickness * dir1;
    fPosition = (invModelViewMat * gl_Position).xyz;
    gl_Position = projectionMat * gl_Position;
    fColor = gColor[1];
    fHorizontal = 1.0f;
    fVertical = 0.0f;
    EmitVertex();

    EndPrimitive();
}
