#version 410

layout(lines) in;
layout(line_strip, max_vertices = 9) out;

uniform mat4 projectionMat;
uniform float thickness;

in vec4 last_point[2];
in vec4 next_point[2];
in vec3 tangent[2];
in vec3 normal[2];
in vec3 bitangent[2];

in float gProgression[2];

out vec3 fColor;

void main() {
    float size_of_vectors = 5.0f * thickness;
    // Normal
    gl_Position = gl_in[0].gl_Position;
    fColor = vec3(0.0f, 1.0f, 0.0f);
    gl_Position = projectionMat * gl_Position;
    EmitVertex();
    gl_Position = gl_in[0].gl_Position + size_of_vectors * vec4(normal[0], 0.0f);
    fColor = vec3(0.0f, 1.0f, 0.0f);
    gl_Position = projectionMat * gl_Position;
    EmitVertex();
    gl_Position = gl_in[0].gl_Position;
    fColor = vec3(0.0f, 1.0f, 0.0f);
    gl_Position = projectionMat * gl_Position;
    EmitVertex();

    // Tangent
    gl_Position = gl_in[0].gl_Position;
    fColor = vec3(1.0f, 0.0f, 0.0f);
    gl_Position = projectionMat * gl_Position;
    EmitVertex();
    gl_Position = gl_in[0].gl_Position + size_of_vectors * vec4(tangent[0], 0.0f);
    fColor = vec3(1.0f, 0.0f, 0.0f);
    gl_Position = projectionMat * gl_Position;
    EmitVertex();
    gl_Position = gl_in[0].gl_Position;
    fColor = vec3(1.0f, 0.0f, 0.0f);
    gl_Position = projectionMat * gl_Position;
    EmitVertex();

    // Bitangent
    gl_Position = gl_in[0].gl_Position;
    fColor = vec3(0.0f, 0.0f, 1.0f);
    gl_Position = projectionMat * gl_Position;
    EmitVertex();
    gl_Position = gl_in[0].gl_Position + size_of_vectors * vec4(bitangent[0], 0.0f);
    fColor = vec3(0.0f, 0.0f, 1.0f);
    gl_Position = projectionMat * gl_Position;
    EmitVertex();
    gl_Position = gl_in[0].gl_Position;
    fColor = vec3(0.0f, 0.0f, 1.0f);
    gl_Position = projectionMat * gl_Position;
    EmitVertex();

    EndPrimitive();
}
