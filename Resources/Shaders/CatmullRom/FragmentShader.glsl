#version 410
#define M_PI 3.14159265358979323846

out vec4 colorResponse;

in vec3 fColor, fTangent, fNormal, fPosition;
in float fHorizontal, fVertical;

uniform int display_mode;
uniform sampler2D shadowTex;
uniform mat4 modelViewMat, projectionMat, depthModelViewMat, depthProjectionMat, normalMat;
uniform float thickness;
uniform bool isShadowOn;
uniform vec3 lightInvDir;

const mat4 bias = mat4(
    0.5, 0.0, 0.0, 0.0,
    0.0, 0.5, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.5, 0.5, 0.5, 1.0);
const float bias_depth = 0.005f;

void main() {
    if (display_mode == 1)
        colorResponse = vec4(fColor, 1.0f);
    else {
        vec3 tangent = normalize(fTangent);

        // Angle of rotation on the tube
        float angle = acos(1.0f - 2.0f * fVertical);

        // Shadow mapping
        float visibility = 1.0;
        if (isShadowOn) {
//            vec4 position_for_light = bias * depthProjectionMat * depthModelViewMat * inverse(modelViewMat) * vec4(fPosition + vec3(0.0f, 0.0f, sin(angle) * 0.01f), 1.0f);
            vec4 position_for_light = bias * depthProjectionMat * depthModelViewMat * vec4(fPosition, 1.0f);
            if (texture(shadowTex, position_for_light.xy).x  <  position_for_light.z - bias_depth) {
                visibility = 0.3f;
            }
        }

        // Normal determination
        vec3 upNormal = normalize(fNormal - dot(tangent, fNormal) * tangent);
        vec3 faceBinormal = cross(tangent, upNormal);
        vec3 normal = cos(angle) * upNormal + sin(angle) * faceBinormal;
        vec3 normalInCameraSpace = (modelViewMat * vec4(normal, 0.0f)).xyz;
//        colorResponse = vec4((normal + 1.0f) / 2.0f, 1.0f);

        // Depth
        float depth = gl_FragCoord.z;
//        colorResponse = vec4(vec3(1.0f - depth), 1.0f);

        // Surface direction //TODO
//        colorResponse = vec4((tangent + 1.0f) / 2.0f, 1.0f);

        // Shading
        float diffuse = 0.8f * clamp(dot(normal, lightInvDir), 0.0f, 1.0f);
        float ambient = 0.2f;
        vec3 albedo = vec3(1.0f, 0.8f, 0.2f); // Yellow
//        vec3 albedo = fColor;
        colorResponse = vec4(diffuse * albedo + ambient * albedo, 1.0f) * visibility;

//        colorResponse = vec4(vec3(angle / M_PI), 1.0f);
//        colorResponse = visibility * vec4(fColor, 1.0f);
//        colorResponse = vec4(fColor * vec3(abs(normalize(fTangent).r)), 1.0f);
//        // BSDF method
//        float phi; // Azimuthal angle
//        float theta_h; // Longitudinal half angle between light and view directions
//        float beta; // Longitudinal width of the specular lobe
//        vec3 k_specular; // Specular color
//        vec3 f_specular = k_specular * cos(phi / 2.0f) * exp(- theta_h * theta_h / 2.0f / beta / beta);

        // TODO: Change value to look tubular
//        gl_FragDepth = gl_FragCoord.z + sin(angle) * thickness;
    }
}
