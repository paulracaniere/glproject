#version 410

layout(location=0) in vec3 cpPosition;
layout(location=1) in float cpProgression;

out float tcProgression;

void main() {
    gl_Position = vec4(cpPosition, 1.0f);

    tcProgression = cpProgression;
}