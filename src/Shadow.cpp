#include <iostream>
#include "Shadow.h"

void Shadow::set_shadow_buffer(GLsizei width, GLsizei height) {
    m_width = width;
    m_height = height;

    glGenFramebuffers(1, &m_framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);

    // Depth texture
    glGenTextures(1, &m_depthTexture);
    glBindTexture(GL_TEXTURE_2D, m_depthTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, m_width, m_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    // Sampling
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    // Attach depth to framebuffer
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_depthTexture, 0);

    // No color buffer is drawn to
    glDrawBuffer(GL_NONE);

    // Always check that our framebuffer is ok
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cerr << "[ERROR] Framebuffer wrongly loaded." << std::endl;
    }
}

void Shadow::bind_buffer() {
    glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);
    glClear(GL_DEPTH_BUFFER_BIT);
    glGetIntegerv(GL_VIEWPORT, previous_viewport);
    glViewport(0, 0, m_width, m_height);
}

void Shadow::unbind_buffer() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(previous_viewport[0], previous_viewport[1], previous_viewport[2], previous_viewport[3]);
}

void Shadow::reset() {
    glDeleteFramebuffers(1, &m_framebuffer);
    glDeleteTextures(1, &m_depthTexture);
}

void Shadow::activateTexture(GLuint index) {
    glActiveTexture(GL_TEXTURE0 + index);
    glBindTexture(GL_TEXTURE_2D, m_depthTexture);
}

void Shadow::deactivateTexture(GLuint index) {
    if (m_depthTexture) {
        glActiveTexture(GL_TEXTURE0 + index);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}