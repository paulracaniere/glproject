#ifndef TEST_GL_SKYBOX_H
#define TEST_GL_SKYBOX_H

#include "Mesh.h"
#include "stb_image.h"

class Skybox {
public:
    explicit Skybox();

    inline ~Skybox() { clear(); }

    void init();

    void render();

    void clear();

    void loadCubeMapTexture(const std::string &right_filename, const std::string &left_filename,
                            const std::string &top_filename, const std::string &bottom_filename,
                            const std::string &back_filename, const std::string &front_filename);

    void activateTexture(GLuint index = 0);

    void deactivateTexture(GLuint index = 0);

private:
    static void loadFaceCubeMap(const std::string &filename, GLenum face);

    GLuint m_vao = 0;
    GLuint m_posVbo = 0;
    GLuint m_tex = 0;

    const std::vector<GLfloat> m_triangle = {
            // positions

            // Front
            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,

            // Left
            -1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,

            // Right
            1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,

            // Back
            -1.0f, -1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,

            // Up
            -1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, -1.0f,

            // Bottom
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f
    };
};


#endif //TEST_GL_SKYBOX_H
