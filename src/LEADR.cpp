#include "LEADR.h"
#include <iostream>
#include <glm/glm.hpp>

#define STB_IMAGE_IMPLEMENTATION

void LEADR::loadHeightMap_8bit(const std::string &filename, bool periodic) {
    int width, height, channels;
    unsigned char *base_height_map = stbi_load(filename.c_str(), &width, &height, &channels, 1);

    switch (channels) {
        case (0):
            std::cerr << "[ERROR] Image has 0 channel." << std::endl;
            return;
        case (1):
            break;
        default:
            std::cerr << "[ERROR] Image has more than 1 channel." << std::endl;
            return;
    }

    std::vector<glm::vec4> LEADR_values0(height * width);
    std::vector<float> LEADR_values1(height * width);

    for (auto y = 0; y < height; y++) {
        for (auto x = 1; x < width - 1; x++) {
            LEADR_values0[y * width + x].x =
                    (float(base_height_map[y * width + x + 1]) - float(base_height_map[y * width + x - 1])) / 255.0f *
                    float(width);
        }
        if (!periodic) {
            LEADR_values0[y * width + 0].x = LEADR_values0[y * width + 1].x;
            LEADR_values0[y * width + width - 1].x = LEADR_values0[y * width + width - 2].x;
        } else {
            LEADR_values0[y * width + 0].x =
                    (float(base_height_map[y * width + 1]) - float(base_height_map[y * width + width - 1])) / 255.0f *
                    float(width);
            LEADR_values0[y * width + width - 1].x =
                    (float(base_height_map[y * width + 0]) - float(base_height_map[y * width + width - 2])) / 255.0f *
                    float(width);
        }
    }

    for (auto x = 0; x < width; x++) {
        for (auto y = 1; y < height - 1; y++) {
            LEADR_values0[y * width + x].y =
                    (float(base_height_map[(y + 1) * width + x]) - float(base_height_map[(y - 1) * width + x])) /
                    255.0f * float(height);
        }
        if (!periodic) {
            LEADR_values0[0 * width + x].y = LEADR_values0[1 * width + x].y;
            LEADR_values0[(height - 1) * width + x].y = LEADR_values0[(height - 2) * width + x].y;
        } else {
            LEADR_values0[0 * width + x].y =
                    (float(base_height_map[1 * width + x]) - float(base_height_map[(height - 1) * width + x])) /
                    255.0f * float(height);
            LEADR_values0[(height - 1) * width + x].y =
                    (float(base_height_map[0 * width + x]) - float(base_height_map[(height - 2) * width + x])) /
                    255.0f * float(height);
        }
    }

    for (auto x = 0; x < width; x++) {
        for (auto y = 0; y < height; y++) {
            LEADR_values0[y * width + x].z = LEADR_values0[y * width + x].x * LEADR_values0[y * width + x].x;
        }
    }

    for (auto x = 0; x < width; x++) {
        for (auto y = 0; y < height; y++) {
            LEADR_values0[y * width + x].w = LEADR_values0[y * width + x].y * LEADR_values0[y * width + x].y;
        }
    }

    for (auto x = 0; x < width; x++) {
        for (auto y = 0; y < height; y++) {
            LEADR_values1[y * width + x] = LEADR_values0[y * width + x].x * LEADR_values0[y * width + x].y;
        }
    }

    LEADR_values0[0] += 1.0f;

    // Load the height map into an OpenGL texture
    glGenTextures(1, &heightTex);
    glBindTexture(GL_TEXTURE_2D, heightTex);
    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 GL_RED, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 GL_RED, // Number of components of image
                 GL_UNSIGNED_BYTE, // Size of each pixel in bits (image encoding)
                 base_height_map); // Array/Pointer of data
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY, GL_MAX_TEXTURE_MAX_ANISOTROPY);

    // Load these 5 moments into two MIP-mapped OpenGL textures
    glGenTextures(1, &LEADRtex0);
    glGenTextures(1, &LEADRtex1);
    glBindTexture(GL_TEXTURE_2D, LEADRtex0);
    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 GL_RGBA32F, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 GL_RGBA, // Number of components of image
                 GL_FLOAT, // Size of each pixel in bits (image encoding)
                 LEADR_values0.data()); // Array/Pointer of data
    // FIXME: MIP-mapping
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY, GL_MAX_TEXTURE_MAX_ANISOTROPY);

    glBindTexture(GL_TEXTURE_2D, LEADRtex1);
    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 GL_R32F, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 GL_RED, // Number of components of image
                 GL_FLOAT, // Size of each pixel in bits (image encoding)
                 LEADR_values1.data()); // Array/Pointer of data
    // FIXME: MIP-mapping
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY, GL_MAX_TEXTURE_MAX_ANISOTROPY);

    glBindTexture(GL_TEXTURE_2D, 0);

    // Freeing the now useless CPU memory
    stbi_image_free(base_height_map);
}

void LEADR::loadHeightMap_16bit(const std::string &filename, bool periodic) {
    int width, height, channels;
    unsigned short *base_height_map = stbi_load_16(filename.c_str(), &width, &height, &channels, 1);

    switch (channels) {
        case (0):
            std::cerr << "[ERROR] Image has 0 channel." << std::endl;
            return;
        case (1):
            break;
        default:
            std::cerr << "[ERROR] Image has more than 1 channel." << std::endl;
            return;
    }

    std::vector<glm::vec4> LEADR_values0(height * width);
    std::vector<float> LEADR_values1(height * width);

    for (auto y = 0; y < height; y++) {
        for (auto x = 1; x < width - 1; x++) {
            LEADR_values0[y * width + x].x =
                    (float(base_height_map[y * width + x + 1]) - float(base_height_map[y * width + x - 1])) /
                    65535.0f * float(width);
        }
        if (!periodic) {
            LEADR_values0[y * width + 0].x = LEADR_values0[y * width + 1].x;
            LEADR_values0[y * width + width - 1].x = LEADR_values0[y * width + width - 2].x;
        } else {
            LEADR_values0[y * width + 0].x =
                    (float(base_height_map[y * width + 1]) - float(base_height_map[y * width + width - 1])) /
                    65535.0f * float(width);
            LEADR_values0[y * width + width - 1].x =
                    (float(base_height_map[y * width + 0]) - float(base_height_map[y * width + width - 2])) /
                    65535.0f * float(width);
        }
    }

    for (auto x = 0; x < width; x++) {
        for (auto y = 1; y < height - 1; y++) {
            LEADR_values0[y * width + x].y =
                    (float(base_height_map[(y + 1) * width + x]) - float(base_height_map[(y - 1) * width + x])) /
                    65535.0f * float(height);
        }
        if (!periodic) {
            LEADR_values0[0 * width + x].y = LEADR_values0[1 * width + x].y;
            LEADR_values0[(height - 1) * width + x].y = LEADR_values0[(height - 2) * width + x].y;
        } else {
            LEADR_values0[0 * width + x].y =
                    (float(base_height_map[1 * width + x]) - float(base_height_map[(height - 1) * width + x])) /
                    65535.0f * float(height);
            LEADR_values0[(height - 1) * width + x].y =
                    (float(base_height_map[0 * width + x]) - float(base_height_map[(height - 2) * width + x])) /
                    65535.0f * float(height);
        }
    }

    for (auto x = 0; x < width; x++) {
        for (auto y = 0; y < height; y++) {
            LEADR_values0[y * width + x].z = LEADR_values0[y * width + x].x * LEADR_values0[y * width + x].x;
        }
    }

    for (auto x = 0; x < width; x++) {
        for (auto y = 0; y < height; y++) {
            LEADR_values0[y * width + x].w = LEADR_values0[y * width + x].y * LEADR_values0[y * width + x].y;
        }
    }

    for (auto x = 0; x < width; x++) {
        for (auto y = 0; y < height; y++) {
            LEADR_values1[y * width + x] = LEADR_values0[y * width + x].x * LEADR_values0[y * width + x].y;
        }
    }

    // Load the height map into an OpenGL texture
    glGenTextures(1, &heightTex);
    glBindTexture(GL_TEXTURE_2D, heightTex);
    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 GL_RED, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 GL_RED, // Number of components of image
                 GL_UNSIGNED_SHORT, // Size of each pixel in bits (image encoding)
                 base_height_map); // Array/Pointer of data
    glGenerateMipmap(GL_TEXTURE_2D);


    // Load these 5 moments into two MIP-mapped OpenGL textures
    glGenTextures(1, &LEADRtex0);
    glGenTextures(1, &LEADRtex1);
    glBindTexture(GL_TEXTURE_2D, LEADRtex0);
    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 GL_RGBA32F, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 GL_RGBA, // Number of components of image
                 GL_FLOAT, // Size of each pixel in bits (image encoding)
                 LEADR_values0.data()); // Array/Pointer of data
    // FIXME: MIP-mapping
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, LEADRtex1);
    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 GL_R32F, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 GL_RED, // Number of components of image
                 GL_FLOAT, // Size of each pixel in bits (image encoding)
                 LEADR_values1.data()); // Array/Pointer of data
    // FIXME: MIP-mapping
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);

    // Freeing the now useless CPU memory
    stbi_image_free(base_height_map);
}

void LEADR::loadHeightMap_32bit(const std::string &filename, bool periodic) {
    int width, height, channels;
    float *base_height_map = stbi_loadf(filename.c_str(), &width, &height, &channels, 1);

    switch (channels) {
        case (0):
            std::cerr << "[ERROR] Image has 0 channel." << std::endl;
            return;
        case (1):
            break;
        default:
            std::cerr << "[ERROR] Image has more than 1 channel." << std::endl;
            return;
    }

    std::vector<glm::vec4> LEADR_values0(height * width);
    float LEADR_values1[height * width];

    for (auto y = 0; y < height; y++) {
        for (auto x = 1; x < width - 1; x++) {
            LEADR_values0[y * width + x].x =
                    (float(base_height_map[y * width + x + 1]) - float(base_height_map[y * width + x - 1])) *
                    float(width);
        }
        if (!periodic) {
            LEADR_values0[y * width + 0].x = LEADR_values0[y * width + 1].x;
            LEADR_values0[y * width + width - 1].x = LEADR_values0[y * width + width - 2].x;
        } else {
            LEADR_values0[y * width + 0].x =
                    (float(base_height_map[y * width + 1]) - float(base_height_map[y * width + width - 1])) *
                    float(width);
            LEADR_values0[y * width + width - 1].x =
                    (float(base_height_map[y * width + 0]) - float(base_height_map[y * width + width - 2])) *
                    float(width);
        }
    }

    for (auto x = 0; x < width; x++) {
        for (auto y = 1; y < height - 1; y++) {
            LEADR_values0[y * width + x].y =
                    (float(base_height_map[(y + 1) * width + x]) - float(base_height_map[(y - 1) * width + x])) *
                    float(height);
        }
        if (!periodic) {
            LEADR_values0[0 * width + x].y = LEADR_values0[1 * width + x].y;
            LEADR_values0[(height - 1) * width + x].y = LEADR_values0[(height - 2) * width + x].y;
        } else {
            LEADR_values0[0 * width + x].y =
                    (float(base_height_map[1 * width + x]) - float(base_height_map[(height - 1) * width + x])) *
                    float(height);
            LEADR_values0[(height - 1) * width + x].y =
                    (float(base_height_map[0 * width + x]) - float(base_height_map[(height - 2) * width + x])) *
                    float(height);
        }
    }

    for (auto x = 0; x < width; x++) {
        for (auto y = 0; y < height; y++) {
            LEADR_values0[y * width + x].z = LEADR_values0[y * width + x].x * LEADR_values0[y * width + x].x;
        }
    }

    for (auto x = 0; x < width; x++) {
        for (auto y = 0; y < height; y++) {
            LEADR_values0[y * width + x].w = LEADR_values0[y * width + x].y * LEADR_values0[y * width + x].y;
        }
    }

    for (auto x = 0; x < width; x++) {
        for (auto y = 0; y < height; y++) {
            LEADR_values1[y * width + x] = LEADR_values0[y * width + x].x * LEADR_values0[y * width + x].y;
        }
    }

    // Load the height map into an OpenGL texture
    glGenTextures(1, &heightTex);
    glBindTexture(GL_TEXTURE_2D, heightTex);
    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 GL_RED, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 GL_RED, // Number of components of image
                 GL_FLOAT, // Size of each pixel in bits (image encoding)
                 base_height_map); // Array/Pointer of data
    glGenerateMipmap(GL_TEXTURE_2D);

    // Load these 5 moments into two MIP-mapped OpenGL textures
    glGenTextures(1, &LEADRtex0);
    glGenTextures(1, &LEADRtex1);
    glBindTexture(GL_TEXTURE_2D, LEADRtex0);
    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 GL_RGBA32F, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 GL_RGBA, // Number of components of image
                 GL_FLOAT, // Size of each pixel in bits (image encoding)
                 LEADR_values0.data()); // Array/Pointer of data
    // FIXME: MIP-mapping
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, LEADRtex1);
    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 GL_R32F, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 GL_RED, // Number of components of image
                 GL_FLOAT, // Size of each pixel in bits (image encoding)
                 LEADR_values1); // Array/Pointer of data
    // FIXME: MIP-mapping
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);

    // Freeing the now useless CPU memory
    stbi_image_free(base_height_map);
}

void LEADR::generateSinusoidalTexture(int sizeOfTexture) {
    float freq = 2.0f * M_PIf32 / float(sizeOfTexture);

    std::vector<glm::vec4> LEADR_values0(sizeOfTexture * sizeOfTexture);
    std::vector<float> LEADR_values1(sizeOfTexture * sizeOfTexture);

    std::vector<float> base_height_map(sizeOfTexture * sizeOfTexture);
    for (auto y = 0; y < sizeOfTexture; y++) {
        for (auto x = 0; x < sizeOfTexture; x++) {
            base_height_map[y * sizeOfTexture + x] = 0.5f * (1.0f + sinf(freq * x));
        }
    }

    for (auto y = 0; y < sizeOfTexture; y++) {
        for (auto x = 1; x < sizeOfTexture - 1; x++) {
            LEADR_values0[y * sizeOfTexture + x].x =
                    (float(base_height_map[y * sizeOfTexture + x + 1]) -
                     float(base_height_map[y * sizeOfTexture + x - 1])) *
                    float(sizeOfTexture);
        }
        LEADR_values0[y * sizeOfTexture + 0].x = LEADR_values0[y * sizeOfTexture + 1].x;
        LEADR_values0[y * sizeOfTexture + sizeOfTexture - 1].x = LEADR_values0[y * sizeOfTexture + sizeOfTexture - 2].x;

    }

    for (auto x = 0; x < sizeOfTexture; x++) {
        for (auto y = 1; y < sizeOfTexture - 1; y++) {
            LEADR_values0[y * sizeOfTexture + x].y =
                    (float(base_height_map[(y + 1) * sizeOfTexture + x]) -
                     float(base_height_map[(y - 1) * sizeOfTexture + x])) *
                    float(sizeOfTexture);
        }
        LEADR_values0[0 * sizeOfTexture + x].y = LEADR_values0[1 * sizeOfTexture + x].y;
        LEADR_values0[(sizeOfTexture - 1) * sizeOfTexture + x].y = LEADR_values0[(sizeOfTexture - 2) * sizeOfTexture +
                                                                                 x].y;

    }

    for (auto x = 0; x < sizeOfTexture; x++) {
        for (auto y = 0; y < sizeOfTexture; y++) {
            LEADR_values0[y * sizeOfTexture + x].z =
                    LEADR_values0[y * sizeOfTexture + x].x * LEADR_values0[y * sizeOfTexture + x].x;
        }
    }

    for (auto x = 0; x < sizeOfTexture; x++) {
        for (auto y = 0; y < sizeOfTexture; y++) {
            LEADR_values0[y * sizeOfTexture + x].w =
                    LEADR_values0[y * sizeOfTexture + x].y * LEADR_values0[y * sizeOfTexture + x].y;
        }
    }

    for (auto x = 0; x < sizeOfTexture; x++) {
        for (auto y = 0; y < sizeOfTexture; y++) {
            LEADR_values1[y * sizeOfTexture + x] =
                    LEADR_values0[y * sizeOfTexture + x].x * LEADR_values0[y * sizeOfTexture + x].y;
        }
    }

    // Load the height map into an OpenGL texture
    glGenTextures(1, &heightTex);
    glBindTexture(GL_TEXTURE_2D, heightTex);
    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 GL_RED, // Number of components of image
                 sizeOfTexture,
                 sizeOfTexture,
                 0, // Always 0
                 GL_RED, // Number of components of image
                 GL_FLOAT, // Size of each pixel in bits (image encoding)
                 base_height_map.data()); // Array/Pointer of data
    glGenerateMipmap(GL_TEXTURE_2D);

    // Load these 5 moments into two MIP-mapped OpenGL textures
    glGenTextures(1, &LEADRtex0);
    glGenTextures(1, &LEADRtex1);
    glBindTexture(GL_TEXTURE_2D, LEADRtex0);
    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 GL_RGBA32F, // Number of components of image
                 sizeOfTexture,
                 sizeOfTexture,
                 0, // Always 0
                 GL_RGBA, // Number of components of image
                 GL_FLOAT, // Size of each pixel in bits (image encoding)
                 LEADR_values0.data()); // Array/Pointer of data
    // FIXME: MIP-mapping
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, LEADRtex1);
    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 GL_R32F, // Number of components of image
                 sizeOfTexture,
                 sizeOfTexture,
                 0, // Always 0
                 GL_RED, // Number of components of image
                 GL_FLOAT, // Size of each pixel in bits (image encoding)
                 LEADR_values1.data()); // Array/Pointer of data
    // FIXME: MIP-mapping
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);
}

void LEADR::activateTextures(GLuint index) {
    glActiveTexture(GL_TEXTURE0 + index);
    glBindTexture(GL_TEXTURE_2D, heightTex);
    glActiveTexture(GL_TEXTURE1 + index);
    glBindTexture(GL_TEXTURE_2D, LEADRtex0);
    glActiveTexture(GL_TEXTURE2 + index);
    glBindTexture(GL_TEXTURE_2D, LEADRtex1);
}

void LEADR::deactivateTextures(GLuint index) {
    if (heightTex) {
        glActiveTexture(GL_TEXTURE0 + index);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    if (LEADRtex0) {
        glActiveTexture(GL_TEXTURE1 + index);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    if (LEADRtex1) {
        glActiveTexture(GL_TEXTURE2 + index);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void LEADR::clear() {
    if (heightTex) {
        glDeleteTextures(1, &heightTex);
        heightTex = 0;
    }
    if (LEADRtex0) {
        glDeleteTextures(1, &LEADRtex0);
        LEADRtex0 = 0;
    }
    if (LEADRtex1) {
        glDeleteTextures(1, &LEADRtex1);
        LEADRtex1 = 0;
    }
}

void LEADR::sampling(int N, float lambda, std::vector<GLfloat> &positions, std::vector<GLfloat> &weights) {
    int mem_size = (N + 1) / 2;

    positions = std::vector<GLfloat>(mem_size);
    weights = std::vector<GLfloat>(mem_size);

    if (N % 2 == 1) {
        positions[0] = 0.0f;
        for (int i = 1; i < mem_size; i++) {
            positions[i] = lambda * GLfloat(i) / GLfloat(N / 2);
        }
    } else {
        for (int i = 0; i < mem_size; i++) {
            positions[i] = lambda * GLfloat(2 * i + 1) / GLfloat(N - 1);
        }
    }

    float total_weight = 0.0f;
    if (N % 2 == 1) {
        total_weight += 1.0f;
    } else {
        total_weight += 2.0f * expf(-0.5f * positions[0] * positions[0]);
    }
    for (int i = 1; i < mem_size; i++) {
        total_weight += 2.0f * expf(-0.5f * positions[i] * positions[i]);
    }
    for (int i = 0; i < mem_size; i++) {
        weights[i] = expf(-0.5f * positions[i] * positions[i]) / total_weight;
    }
}
