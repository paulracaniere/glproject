#ifndef TEST_GL_SHADOW_H
#define TEST_GL_SHADOW_H

#include <glad/glad.h>


class Shadow {
public:
    void set_shadow_buffer(GLsizei width, GLsizei height);

    void bind_buffer();

    void unbind_buffer();

    void activateTexture(GLuint index);

    void deactivateTexture(GLuint index);

    void reset();

private:
    GLuint m_framebuffer, m_depthTexture;
    GLsizei m_width, m_height;
    GLint previous_viewport[4];
};


#endif //TEST_GL_SHADOW_H
