#include "CatmullRomCurve.h"

#include <cstdint>
#include <cstdio>
#include <string>
#include <iostream>
#include <random>
#include <algorithm>

void CatmullRomCurve::loadLineSample() {
    m_controlPoints = {
            {-2.0f, 0.0f, 0.0f},
            {-1.0f, 0.0f, 0.0f},
            {-0.0f, 0.0f, 0.0f},
            {+1.0f, 0.0f, 0.0f},
            {+2.0f, 0.0f, 0.0f},
    };
    m_firstControlPoint = {0, 5};
    m_isLoop = {false};
}

void CatmullRomCurve::loadSimpleExample() {
    m_controlPoints = {{0.0f,  -1.0f, 0.0f},
                       {-1.0f, 0.0,   0.0f},
                       {0.0f,  1.0f,  0.0f},
                       {1.0f,  0.0f,  0.0f},
                       {0.0f,  -1.0f, 0.0f},
                       {-1.0f, 0.0,   0.0f},
                       {0.0f,  1.0f,  0.0f}};

    m_firstControlPoint = {0, 7};
    m_isLoop = {false};
}

bool CatmullRomCurve::loadFromBCCFile(const std::string &file) {
    struct BCCHeader {
        char sign[3];
        unsigned char byteCount;
        char curveType[2];
        char dimensions;
        char upDimension;
        uint64_t curveCount;
        uint64_t totalControlPointCount;
        char fileInfo[40];
    } header{};

    FILE *pFile = fopen(file.c_str(), "rb");
    if (pFile == nullptr) {
        std::cerr << "Could not read file" << std::endl;
        return false;
    }

    fread(&header, sizeof(header), 1, pFile);

    if (header.sign[0] != 'B') return false; // Invalid file signature
    if (header.sign[1] != 'C') return false; // Invalid file signature
    if (header.sign[2] != 'C') return false; // Invalid file signature
    if (header.byteCount != 0x44) return false; // Only supporting 4-byte integers and floats
    if (header.curveType[0] != 'C') return false; // Not a Catmull-Rom curve
    if (header.curveType[1] != '0') return false; // Not uniform parametrization
    if (header.dimensions != 3) return false; // Only curves in 3D

    m_controlPoints.resize(header.totalControlPointCount);
    m_firstControlPoint.resize(header.curveCount + 1);
    m_isLoop.resize(header.curveCount);

    glm::vec3 *cp = &m_controlPoints[0];

    int prevCP = 0;
    for (uint64_t i = 0; i < header.curveCount; i++) {
        int curveControlPointCount;
        fread(&curveControlPointCount, sizeof(int), 1, pFile);
        m_isLoop[i] = curveControlPointCount < 0;
        if (curveControlPointCount < 0) curveControlPointCount = -curveControlPointCount;

        fread(cp, sizeof(glm::vec3), curveControlPointCount, pFile);
        cp += curveControlPointCount;
        m_firstControlPoint[i] = prevCP;
        prevCP += curveControlPointCount;
    }
    m_firstControlPoint[header.curveCount] = prevCP;

    return true;
}

void CatmullRomCurve::computeBoundingSphere(glm::vec3 &center, float &radius) const {
    center = glm::vec3(0.0);
    radius = 0.f;
    for (const auto &p : m_controlPoints)
        center += p;
    center /= m_controlPoints.size();
    for (const auto &p : m_controlPoints)
        radius = std::max(radius, distance(center, p));
}

void CatmullRomCurve::computeIbo() {
    m_indices.clear();
    for (size_t i = 0; i < m_isLoop.size(); i++) {
        if (m_isLoop[i])
            m_indices.emplace_back(m_firstControlPoint[i + 1] - 1, m_firstControlPoint[i],
                                   m_firstControlPoint[i] + 1, m_firstControlPoint[i] + 2);
        for (GLuint j = m_firstControlPoint[i]; j < m_firstControlPoint[i + 1] - 3; j++) {
            m_indices.emplace_back(j, j + 1, j + 2, j + 3);
        }
        if (m_isLoop[i]) {
            m_indices.emplace_back(m_firstControlPoint[i + 1] - 3, m_firstControlPoint[i + 1] - 2,
                                   m_firstControlPoint[i + 1] - 1, m_firstControlPoint[i]);
            m_indices.emplace_back(m_firstControlPoint[i + 1] - 2, m_firstControlPoint[i + 1] - 1,
                                   m_firstControlPoint[i], m_firstControlPoint[i] + 1);
        }
    }
}

void CatmullRomCurve::computeProgression() {
    m_progression.clear();
    for (size_t i = 0; i < m_isLoop.size(); i++) {
        for (GLuint j = 0; j < m_firstControlPoint[i + 1] - m_firstControlPoint[i]; j++) {
            m_progression.push_back(GLfloat(j));
        }
    }
}

void CatmullRomCurve::computeRadiiAndInitialAngles() {
    // Random generation
    std::random_device rd;
    std::mt19937 generator(rd());
    std::normal_distribution<GLfloat> normal(0, 0.5);

    std::vector<GLfloat> radii, angles;

    // Generating
    for (auto i = 0; i < 64; i++) {
        glm::vec2 point = {normal(generator), normal(generator)};

        radii.push_back(glm::length(point));

        if (point.x > 0) angles.push_back(atanf(point.y / point.x));
        else if (point.y > 0) angles.push_back(M_PI + atanf(point.y / point.x));
        else angles.push_back(-M_PI + atanf(point.y / point.x));
    }

    // Sorting by decreasing radius
    std::vector<size_t> idx(radii.size());
    std::iota(idx.begin(), idx.end(), 0);
    std::stable_sort(idx.begin(), idx.end(), [&radii](size_t i1, size_t i2) { return radii[i1] > radii[i2]; });
    for (auto i : idx) {
        m_radiiAndAngles.emplace_back(std::fminf(radii[i], 2.0f), angles[i]);
    }

    // Loading in texture
    glGenTextures(1, &m_radiiAnglesTex);
    glBindTexture(GL_TEXTURE_1D, m_radiiAnglesTex);
    glTexImage1D(GL_TEXTURE_1D, // Generates 1D texture in GPU
                 0, // LoD at closest level
                 GL_RG32F, // Number of components of image
                 64, // Width of the texture
                 0, // Always 0
                 GL_RG, // Number of components of image
                 GL_FLOAT, // Size of each pixel in bits (image encoding)
                 m_radiiAndAngles.data()); // Array/Pointer of data
    glGenerateMipmap(GL_TEXTURE_1D);
    glBindTexture(GL_TEXTURE_1D, 0);
}

void CatmullRomCurve::activateTextures(GLuint index) const {
    glActiveTexture(GL_TEXTURE0 + index);
    glBindTexture(GL_TEXTURE_1D, m_radiiAnglesTex);
}

void CatmullRomCurve::deactivateTexture(GLuint index) const {
    if (m_radiiAnglesTex) {
        glActiveTexture(GL_TEXTURE0 + index);
        glBindTexture(GL_TEXTURE_1D, 0);
    }
}

void CatmullRomCurve::init() {
    computeIbo();
    computeProgression();
    computeRadiiAndInitialAngles();

    size_t bufferSize;

    glGenBuffers(1, &m_posVbo); // Generate a GPU buffer to store the positions of the control points
    bufferSize = sizeof(glm::vec3) * m_controlPoints.size();
    glBindBuffer(GL_ARRAY_BUFFER, m_posVbo);
    glBufferData(GL_ARRAY_BUFFER, bufferSize, m_controlPoints.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &m_progVbo); // Generate a GPU buffer to store the positions of the control points
    bufferSize = sizeof(GLfloat) * m_progression.size();
    glBindBuffer(GL_ARRAY_BUFFER, m_progVbo);
    glBufferData(GL_ARRAY_BUFFER, bufferSize, m_progression.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Same for the index buffer, that stores the list of indices of the triangles forming the mesh
    glGenBuffers(1, &m_ibo);
    bufferSize = sizeof(glm::uvec4) * m_indices.size();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, bufferSize, m_indices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Create a single handle that joins together attributes and connectivity
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m_posVbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nullptr);
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, m_progVbo);
    glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat), nullptr);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    glBindVertexArray(0); // Deactivates the VAO just created. Will be activated at rendering time.
}

void CatmullRomCurve::render(int nb_vertices) {
    // Activate the VAO storing geometry data
    glBindVertexArray(m_vao);

    // Call for rendering: stream the current GPU geometry through the current GPU program
    glPatchParameteri(GL_PATCH_VERTICES, 4);
    glDrawElements(GL_PATCHES, 4 * (nb_vertices < 0 ? m_indices.size() : nb_vertices), GL_UNSIGNED_INT, nullptr);

    glBindVertexArray(0);
}

void CatmullRomCurve::clear() {
    m_controlPoints.clear();
    m_isLoop.clear();
    m_firstControlPoint.clear();
    m_progression.clear();
    if (m_vao) {
        glDeleteVertexArrays(1, &m_vao);
        m_vao = 0;
    }
    if (m_posVbo) {
        glDeleteBuffers(1, &m_posVbo);
        m_posVbo = 0;
    }
    if (m_progVbo) {
        glDeleteVertexArrays(1, &m_progVbo);
        m_progVbo = 0;
    }
    if (m_ibo) {
        glDeleteBuffers(1, &m_ibo);
        m_ibo = 0;
    }
}
