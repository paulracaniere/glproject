#define _USE_MATH_DEFINES
#include "Mesh.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <algorithm>
#include <iostream>
#include <cmath>

#define TEST_OPENGL_ERROR()                                                             \
  do {                                    \
    GLenum err = glGetError();                                            \
    if (err != GL_NO_ERROR) std::cerr << "OpenGL ERROR! #"<< err <<" in " << __FILE__ <<" at line "<< __LINE__ << std::endl;      \
  } while(0)

using namespace std;

Mesh::~Mesh() {
    clear();
}

void Mesh::computePlanarParameterization() {
    m_vertexTexCoords = std::vector<glm::vec2>();
    float maxX = 0, maxY = 0, minX = 0, minY = 0;
    for (glm::vec3 vertex : m_vertexPositions) {
        if (vertex[0] > maxX) maxX = vertex[0];
        if (vertex[0] < minX) minX = vertex[0];
        if (vertex[1] > maxY) maxY = vertex[1];
        if (vertex[1] < minY) minY = vertex[1];
    }
    float max = std::max(maxX, maxY);
    float min = std::min(minX, minY);
    for (glm::vec3 vertex : m_vertexPositions) {
        m_vertexTexCoords.emplace_back((vertex[0] - min) / (max - min), (vertex[1] - min) / (max - min));
    }
}

void Mesh::computeBoundingSphere(glm::vec3 &center, float &radius) const {
    center = glm::vec3(0.0);
    radius = 0.f;
    for (const auto &p : m_vertexPositions)
        center += p;
    center /= m_vertexPositions.size();
    for (const auto &p : m_vertexPositions)
        radius = std::max(radius, distance(center, p));
}

void Mesh::recomputePerVertexNormals(bool angleBased) {
    m_vertexNormals.clear();
    // Change the following code to compute a proper per-vertex normal
    m_vertexNormals.resize(m_vertexPositions.size(), glm::vec3(0.0, 0.0, 0.0));

    for (glm::uvec3 triangle : m_triangleIndices) {
        glm::vec3 normal = glm::cross(m_vertexPositions[triangle[1]] - m_vertexPositions[triangle[0]],
                                      m_vertexPositions[triangle[2]] - m_vertexPositions[triangle[0]]);
        m_vertexNormals[triangle[0]] += normal;
        m_vertexNormals[triangle[1]] += normal;
        m_vertexNormals[triangle[2]] += normal;
    }

    for (glm::vec3 &normal : m_vertexNormals) {
        normal = glm::normalize(normal);
    }
}

void Mesh::flipNormals() {
    for (auto &normal : m_vertexNormals) {
        normal = -normal;
    }

    for (auto &triangleIndex : m_triangleIndices) {
        glm::u32 flip0 = triangleIndex[0];
        triangleIndex[0] = triangleIndex[1];
        triangleIndex[1] = flip0;
    }
}

void Mesh::recomputeTangentSpace() {
    m_vertexTangents.clear();
    m_vertexBinormals.clear();

    m_vertexTangents.resize(m_vertexPositions.size(), glm::vec3(0.0, 0.0, 0.0));
    m_vertexBinormals.resize(m_vertexPositions.size(), glm::vec3(0.0, 0.0, 0.0));

    for (const glm::uvec3 &triangle : m_triangleIndices) {
        glm::vec3 p0 = m_vertexPositions[triangle[0]];
        glm::vec3 p1 = m_vertexPositions[triangle[1]];
        glm::vec3 p2 = m_vertexPositions[triangle[2]];

        glm::vec3 q1 = p1 - p0;
        glm::vec3 q2 = p2 - p0;

        float s1 = m_vertexTexCoords[triangle[1]][0] - m_vertexTexCoords[triangle[0]][0];
        float s2 = m_vertexTexCoords[triangle[2]][0] - m_vertexTexCoords[triangle[0]][0];
        float t1 = m_vertexTexCoords[triangle[1]][1] - m_vertexTexCoords[triangle[0]][1];
        float t2 = m_vertexTexCoords[triangle[2]][1] - m_vertexTexCoords[triangle[0]][1];

        glm::vec3 B = (s2 * q1 - s1 * q2) / (q2 * t1 - s1 * t2);
        glm::vec3 T = (t2 * q1 - t1 * q2) / (t2 * s1 - t1 * s2);

        m_vertexTangents[triangle[0]] += T;
        m_vertexTangents[triangle[1]] += T;
        m_vertexTangents[triangle[2]] += T;

        m_vertexBinormals[triangle[0]] += B;
        m_vertexBinormals[triangle[1]] += B;
        m_vertexBinormals[triangle[2]] += B;
    }

    for (int i = 0; i < m_vertexPositions.size(); i++) {
        m_vertexTangents[i] =
                m_vertexTangents[i] - glm::dot(m_vertexNormals[i], m_vertexTangents[i]) * m_vertexNormals[i];
        m_vertexBinormals[i] = m_vertexBinormals[i]
                               - glm::dot(m_vertexNormals[i], m_vertexBinormals[i]) * m_vertexBinormals[i]
                               - glm::dot(m_vertexTangents[i], m_vertexBinormals[i]) *
                                 glm::normalize(m_vertexTangents[i]);
        m_vertexTangents[i] = glm::normalize(m_vertexTangents[i]);
        m_vertexBinormals[i] = glm::normalize(m_vertexBinormals[i]);
    }

}

void Mesh::init() {
    size_t vertexBufferSize;
    size_t texCoordBufferSize;
    if (!m_vertexPositions.empty()) {
        vertexBufferSize = sizeof(glm::vec3) * m_vertexPositions.size();

        glGenBuffers(1, &m_posVbo); // Generate a GPU buffer to store the positions of the vertices
        // Gather the size of the buffer from the CPU-side vector
        glBindBuffer(GL_ARRAY_BUFFER, m_posVbo);
        glBufferData(GL_ARRAY_BUFFER, vertexBufferSize, m_vertexPositions.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    if (!m_vertexNormals.empty()) {
        vertexBufferSize = sizeof(glm::vec3) * m_vertexNormals.size();

        glGenBuffers(1, &m_normalVbo); // Same for normal
        glBindBuffer(GL_ARRAY_BUFFER, m_normalVbo);
        glBufferData(GL_ARRAY_BUFFER, vertexBufferSize, m_vertexNormals.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    if (!m_vertexTexCoords.empty()) {
        texCoordBufferSize = sizeof(glm::vec2) * m_vertexTexCoords.size();

        glGenBuffers(1, &m_texCoordVbo); // Same for texture coordinates
        glBindBuffer(GL_ARRAY_BUFFER, m_texCoordVbo);
        glBufferData(GL_ARRAY_BUFFER, texCoordBufferSize, m_vertexTexCoords.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    if (!m_vertexTangents.empty()) {
        vertexBufferSize = sizeof(glm::vec3) * m_vertexTangents.size();

        glGenBuffers(1, &m_tangentVbo); // Same for normal
        glBindBuffer(GL_ARRAY_BUFFER, m_tangentVbo);
        glBufferData(GL_ARRAY_BUFFER, vertexBufferSize, m_vertexTangents.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    if (!m_vertexBinormals.empty()) {
        vertexBufferSize = sizeof(glm::vec3) * m_vertexBinormals.size();

        glGenBuffers(1, &m_binormalVbo); // Same for normal
        glBindBuffer(GL_ARRAY_BUFFER, m_binormalVbo);
        glBufferData(GL_ARRAY_BUFFER, vertexBufferSize, m_vertexBinormals.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    if (!m_triangleIndices.empty()) {
        // Same for the index buffer, that stores the list of indices of the triangles forming the mesh
        glGenBuffers(1, &m_ibo);
        size_t indexBufferSize = sizeof(glm::uvec3) * m_triangleIndices.size();
        glBindBuffer(GL_ARRAY_BUFFER, m_ibo);
        glBufferData(GL_ARRAY_BUFFER, indexBufferSize, m_triangleIndices.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    // Create a single handle that joins together attributes (vertex positions, normals) and connectivity (triangles indices)
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);
    if (m_posVbo) {
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, m_posVbo);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nullptr);
    }
    if (m_normalVbo) {
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, m_normalVbo);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nullptr);
    }
    if (m_tangentVbo) {
        glEnableVertexAttribArray(2);
        glBindBuffer(GL_ARRAY_BUFFER, m_tangentVbo);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nullptr);
    }
    if (m_binormalVbo) {
        glEnableVertexAttribArray(3);
        glBindBuffer(GL_ARRAY_BUFFER, m_binormalVbo);
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nullptr);
    }
    if (m_texCoordVbo) {
        glEnableVertexAttribArray(4);
        glBindBuffer(GL_ARRAY_BUFFER, m_texCoordVbo);
        glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), nullptr);
    }
    if (m_ibo) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    }
    glBindVertexArray(0); // Deactivates the VAO just created. Will be activated at rendering time.
}

void Mesh::loadDiffuseMap_8bit(const std::string &filename) {
    int width, height, numComponents;
    unsigned char *data = stbi_load(filename.c_str(),
                                    &width,
                                    &height,
                                    &numComponents, // 1 for a 8 bit greyscale image, 3 for 24bits RGB image
                                    0);

    glGenTextures(1, &m_diffuseTexId);

    glBindTexture(GL_TEXTURE_2D, m_diffuseTexId);

    GLenum format = 0;
    switch (numComponents) {
        case (1):
            format = GL_RED;
            break;
        case (3):
            format = GL_RGB;
            break;
        default:
            format = GL_RGBA;
            break;
    }

    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 format, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 format, // Number of components of image
                 GL_UNSIGNED_BYTE, // Size of each pixel in bits (image encoding)
                 data); // Array/Pointer of data

    // MIP-mapping is generated
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY, GL_MAX_TEXTURE_MAX_ANISOTROPY);

    glBindTexture(GL_TEXTURE_2D, 0);

    // Freeing the now useless CPU memory
    stbi_image_free(data);
}

void Mesh::loadNormalMap_8bit(const std::string &filename) {
    int width, height, numComponents;
    unsigned char *data = stbi_load(filename.c_str(),
                                    &width,
                                    &height,
                                    &numComponents, // 1 for a 8 bit greyscale image, 3 for 24bits RGB image
                                    0);

    glGenTextures(1, &m_normalTexId);

    glBindTexture(GL_TEXTURE_2D, m_normalTexId);

    GLenum format = 0;
    switch (numComponents) {
        case (1):
            format = GL_RED;
            break;
        case (3):
            format = GL_RGB;
            break;
        default:
            format = GL_RGBA;
            break;
    }

    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 format, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 format, // Number of components of image
                 GL_UNSIGNED_BYTE, // Size of each pixel in bits (image encoding)
                 data); // Array/Pointer of data

    // MIP-mapping is generated
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);

    // Freeing the now useless CPU memory
    stbi_image_free(data);
}

void Mesh::loadSpecularMap_8bit(const std::string &filename) {
    int width, height, numComponents;
    unsigned char *data = stbi_load(filename.c_str(),
                                    &width,
                                    &height,
                                    &numComponents, // 1 for a 8 bit greyscale image, 3 for 24bits RGB image
                                    0);

    glGenTextures(1, &m_specularTexId);

    glBindTexture(GL_TEXTURE_2D, m_specularTexId);

    GLenum format = 0;
    switch (numComponents) {
        case (1):
            format = GL_RED;
            break;
        case (3):
            format = GL_RGB;
            break;
        default:
            format = GL_RGBA;
            break;
    }

    glTexImage2D(GL_TEXTURE_2D, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 format, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 format, // Number of components of image
                 GL_UNSIGNED_BYTE, // Size of each pixel in bits (image encoding)
                 data); // Array/Pointer of data

    // MIP-mapping is generated
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);

    // Freeing the now useless CPU memory
    stbi_image_free(data);
}

void Mesh::activateTextures(int index) {
    if (m_diffuseTexId) {
        glActiveTexture(GL_TEXTURE0 + index);
        glBindTexture(GL_TEXTURE_2D, m_diffuseTexId);
    }
    if (m_normalTexId) {
        glActiveTexture(GL_TEXTURE1 + index);
        glBindTexture(GL_TEXTURE_2D, m_normalTexId);
    }
    if (m_specularTexId) {
        glActiveTexture(GL_TEXTURE2 + index);
        glBindTexture(GL_TEXTURE_2D, m_specularTexId);
    }
}

void Mesh::deactivateTextures(int index) {
    if (m_diffuseTexId) {
        glActiveTexture(GL_TEXTURE0 + index);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    if (m_normalTexId) {
        glActiveTexture(GL_TEXTURE1 + index);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    if (m_specularTexId) {
        glActiveTexture(GL_TEXTURE2 + index);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void Mesh::render() {
    // Activate the VAO storing geometry data
    TEST_OPENGL_ERROR();

    glBindVertexArray(m_vao);

    TEST_OPENGL_ERROR();

    // Call for rendering: stream the current GPU geometry through the current GPU program
    if (is_straight_quads) {
        glPatchParameteri(GL_PATCH_VERTICES, 4);
        TEST_OPENGL_ERROR();
        glDrawArrays(GL_PATCHES, 0, m_vertexPositions.size());
        TEST_OPENGL_ERROR();
    } else {
        glDrawElements(GL_TRIANGLES, static_cast<GLsizei> (m_triangleIndices.size() * 3), GL_UNSIGNED_INT, nullptr);
    }

    glBindVertexArray(0);
}

void Mesh::clear() {
    m_vertexPositions.clear();
    m_vertexNormals.clear();
    m_vertexTangents.clear();
    m_vertexBinormals.clear();
    m_vertexTexCoords.clear();
    m_triangleIndices.clear();
    if (m_vao) {
        glDeleteVertexArrays(1, &m_vao);
        m_vao = 0;
    }
    if (m_posVbo) {
        glDeleteBuffers(1, &m_posVbo);
        m_posVbo = 0;
    }
    if (m_normalVbo) {
        glDeleteBuffers(1, &m_normalVbo);
        m_normalVbo = 0;
    }
    if (m_texCoordVbo) {
        glDeleteBuffers(1, &m_texCoordVbo);
        m_texCoordVbo = 0;
    }
    if (m_tangentVbo) {
        glDeleteBuffers(1, &m_tangentVbo);
        m_tangentVbo = 0;
    }
    if (m_binormalVbo) {
        glDeleteBuffers(1, &m_binormalVbo);
        m_binormalVbo = 0;
    }
    if (m_ibo) {
        glDeleteBuffers(1, &m_ibo);
        m_ibo = 0;
    }
}

std::shared_ptr<Mesh> Mesh::create_area(unsigned int sub) {
    auto res = std::make_shared<Mesh>();

    std::vector<glm::vec3> vPos;
    std::vector<glm::vec2> vUVs;
    auto subf = float(sub);

    for (int i = 0; i < sub; i++) {
        for (int j = 0; j < sub; j++) {
            // Positions
//            vPos.emplace_back(float(i) / subf, 0.0f, float(j) / subf);
//            vPos.emplace_back(float(i) / subf, 0.0f, float(j + 1) / subf);
//            vPos.emplace_back(float(i + 1) / subf, 0.0f, float(j + 1) / subf);
//            vPos.emplace_back(float(i + 1) / subf, 0.0f, float(j) / subf);

            vPos.emplace_back(float(i) / subf - 0.5f, float(j) / subf - 0.5f, 0.0f);
            vPos.emplace_back(float(i + 1) / subf - 0.5f, float(j) / subf - 0.5f, 0.0f);
            vPos.emplace_back(float(i + 1) / subf - 0.5f, float(j + 1) / subf - 0.5f, 0.0f);
            vPos.emplace_back(float(i) / subf - 0.5f, float(j + 1) / subf - 0.5f, 0.0f);

//            vPos.emplace_back(float(i) / subf - 0.5f, float(j) / subf - 0.5f, 0.0f);
//            vPos.emplace_back(float(i) / subf - 0.5f, float(j + 1) / subf - 0.5f, 0.0f);
//            vPos.emplace_back(float(i + 1) / subf - 0.5f, float(j + 1) / subf - 0.5f, 0.0f);
//            vPos.emplace_back(float(i + 1) / subf - 0.5f, float(j) / subf - 0.5f, 0.0f);

            // Texture coordinates
//            vUVs.emplace_back(float(i) / subf, float(j) / subf);
//            vUVs.emplace_back(float(i) / subf, float(j + 1) / subf);
//            vUVs.emplace_back(float(i + 1) / subf, float(j + 1) / subf);
//            vUVs.emplace_back(float(i + 1) / subf, float(j) / subf);

            vUVs.emplace_back(float(i) / subf, float(j) / subf);
            vUVs.emplace_back(float(i + 1) / subf, float(j) / subf);
            vUVs.emplace_back(float(i + 1) / subf, float(j + 1) / subf);
            vUVs.emplace_back(float(i) / subf, float(j + 1) / subf);

        }
    }

    res->vertexPositions() = vPos;
    res->vertexTexCoords() = vUVs;

    // Fills all with up-going normals
//    res->vertexNormals() = std::vector<glm::vec3>(((unsigned int) 1 << sub) * 4, {0.0f, 1.0f, 0.0});
//    res->vertexTangents() = std::vector<glm::vec3>(((unsigned int) 1 << sub) * 4, {1.0f, 0.0f, 0.0});
//    res->vertexBinormals() = std::vector<glm::vec3>(((unsigned int) 1 << sub) * 4, {0.0f, 0.0f, -1.0});

    res->vertexNormals() = std::vector<glm::vec3>(((unsigned int) 1 << sub) * 4, {0.0f, 0.0f, 1.0f});
    res->vertexTangents() = std::vector<glm::vec3>(((unsigned int) 1 << sub) * 4, {1.0f, 0.0f, 0.0f});
    res->vertexBinormals() = std::vector<glm::vec3>(((unsigned int) 1 << sub) * 4, {0.0f, 1.0f, 0.0f});

//    res->vertexNormals() = std::vector<glm::vec3>(((unsigned int) 1 << sub) * 4, {0.0f, 0.0f, -1.0f});
//    res->vertexTangents() = std::vector<glm::vec3>(((unsigned int) 1 << sub) * 4, {-1.0f, 0.0f, 0.0f});
//    res->vertexBinormals() = std::vector<glm::vec3>(((unsigned int) 1 << sub) * 4, {0.0f, 1.0f, 0.0f});

    res->is_straight_quads = true;

    return res;
}

std::shared_ptr<Mesh> Mesh::create_sphere(unsigned int sub) {
    auto res = std::make_shared<Mesh>();

    std::vector<glm::vec3> vPos;
    std::vector<glm::vec2> vUVs;
    std::vector<glm::vec3> vNor;
    std::vector<glm::vec3> vTan;
    std::vector<glm::vec3> vBin;
    auto subf = float(sub);

    for (int i = 0; i < sub; i++) {
        for (int j = 0; j < sub; j++) {
            // Positions
            vPos.emplace_back(
                sin(float(j) / subf * M_PI) * sin(float(i) / subf * M_PI * 2.0f),
                cos(float(j) / subf * M_PI),
                sin(float(j) / subf * M_PI) * -cos(float(i) / subf * M_PI * 2.0f));
            vPos.emplace_back(
                sin(float(j) / subf * M_PI) * sin(float(i + 1) / subf * M_PI * 2.0f),
                cos(float(j) / subf * M_PI),
                sin(float(j) / subf * M_PI) * -cos(float(i + 1) / subf * M_PI * 2.0f));
            vPos.emplace_back(
                sin(float(j + 1) / subf * M_PI) * sin(float(i + 1) / subf * M_PI * 2.0f),
                cos(float(j + 1) / subf * M_PI),
                sin(float(j + 1) / subf * M_PI) * -cos(float(i + 1) / subf * M_PI * 2.0f));
            vPos.emplace_back(
                sin(float(j + 1) / subf * M_PI) * sin(float(i) / subf * M_PI * 2.0f),
                cos(float(j + 1) / subf * M_PI),
                sin(float(j + 1) / subf * M_PI) * -cos(float(i) / subf * M_PI * 2.0f));

            // Texture coordinates
            vUVs.emplace_back(float(i) / subf, float(j) / subf);
            vUVs.emplace_back(float(i + 1) / subf, float(j) / subf);
            vUVs.emplace_back(float(i + 1) / subf, float(j + 1) / subf);
            vUVs.emplace_back(float(i) / subf, float(j + 1) / subf);

            // Normals
            vNor.emplace_back(
                sin(float(j) / subf * M_PI) * sin(float(i) / subf * M_PI * 2.0f),
                cos(float(j) / subf * M_PI),
                sin(float(j) / subf * M_PI) * -cos(float(i) / subf * M_PI * 2.0f));
            vNor.emplace_back(
                sin(float(j) / subf * M_PI) * sin(float(i + 1) / subf * M_PI * 2.0f),
                cos(float(j) / subf * M_PI),
                sin(float(j) / subf * M_PI) * -cos(float(i + 1) / subf * M_PI * 2.0f));
            vNor.emplace_back(
                sin(float(j + 1) / subf * M_PI) * sin(float(i + 1) / subf * M_PI * 2.0f),
                cos(float(j + 1) / subf * M_PI),
                sin(float(j + 1) / subf * M_PI) * -cos(float(i + 1) / subf * M_PI * 2.0f));
            vNor.emplace_back(
                sin(float(j + 1) / subf * M_PI) * sin(float(i) / subf * M_PI * 2.0f),
                cos(float(j + 1) / subf * M_PI),
                sin(float(j + 1) / subf * M_PI) * -cos(float(i) / subf * M_PI * 2.0f));

            // Tangents
            vTan.emplace_back(
                sin(float(j) / subf * M_PI) * cos(float(i) / subf * M_PI * 2.0f),
                0.0f,
                sin(float(j) / subf * M_PI) * sin(float(i) / subf * M_PI * 2.0f));
            vTan.emplace_back(
                sin(float(j) / subf * M_PI) * cos(float(i + 1) / subf * M_PI * 2.0f),
                0.0f,
                sin(float(j) / subf * M_PI) * sin(float(i + 1) / subf * M_PI * 2.0f));
            vTan.emplace_back(
                sin(float(j + 1) / subf * M_PI) * cos(float(i + 1) / subf * M_PI * 2.0f),
                0.0f,
                sin(float(j + 1) / subf * M_PI) * sin(float(i + 1) / subf * M_PI * 2.0f));
            vTan.emplace_back(
                sin(float(j + 1) / subf * M_PI) * cos(float(i) / subf * M_PI * 2.0f),
                0.0f,
                sin(float(j + 1) / subf * M_PI) * sin(float(i) / subf * M_PI * 2.0f));

            // Binormal
            vBin.emplace_back(
                cos(float(j) / subf * M_PI) * sin(float(i) / subf * M_PI * 2.0f),
                -sin(float(j) / subf),
                cos(float(j) / subf * M_PI) * -cos(float(i) / subf * M_PI * 2.0f));
            vBin.emplace_back(
                cos(float(j) / subf * M_PI) * sin(float(i + 1) / subf * M_PI * 2.0f),
                -sin(float(j) / subf * M_PI),
                cos(float(j) / subf * M_PI) * -cos(float(i + 1) / subf * M_PI * 2.0f));
            vBin.emplace_back(
                cos(float(j + 1) / subf * M_PI) * sin(float(i + 1) / subf * M_PI * 2.0f),
                -sin(float(j + 1) / subf * M_PI),
                cos(float(j + 1) / subf * M_PI) * -cos(float(i + 1) / subf * M_PI * 2.0f));
            vBin.emplace_back(
                cos(float(j + 1) / subf * M_PI) * sin(float(i) / subf * M_PI * 2.0f),
                -sin(float(j + 1) / subf * M_PI),
                cos(float(j + 1) / subf * M_PI) * -cos(float(i) / subf * M_PI * 2.0f));
        }
    }

    res->vertexPositions() = vPos;
    res->vertexTexCoords() = vUVs;
    res->vertexNormals() = vNor;
    res->vertexTangents() = vTan;
    res->vertexBinormals() = vBin;

    res->is_straight_quads = true;

    return res;
}
