#ifndef TEST_GL_LEADR_H
#define TEST_GL_LEADR_H

#include <string>
#include <glad/glad.h>
#include <vector>
#include "stb_image.h"

class LEADR {
public:
    void loadHeightMap_8bit(const std::string &filename, bool periodic = true);

    void loadHeightMap_16bit(const std::string &filename, bool periodic = true);

    void loadHeightMap_32bit(const std::string &filename, bool periodic = true);

    void generateSinusoidalTexture(int sizeOfTexture);

    void activateTextures(GLuint index = 0);

    void deactivateTextures(GLuint index = 0);

    void clear();

    static void sampling(int N, float lambda, std::vector<GLfloat> &positions, std::vector<GLfloat> &weights);

    ~LEADR() { clear(); }

private:
    GLuint heightTex, LEADRtex0, LEADRtex1;
};


#endif //TEST_GL_LEADR_H
