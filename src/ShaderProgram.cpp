#include "ShaderProgram.h"

#include <iostream>
#include <fstream>
#include <sstream>

#include <exception>
#include <ios>

using namespace std;

#include <vector>

#define TEST_OPENGL_ERROR()                                                             \
  do {                                    \
    GLenum err = glGetError();                                            \
    if (err != GL_NO_ERROR) std::cerr << "OpenGL ERROR! #"<< err <<" at line "<< __LINE__ << " " << __FILE__ <<std::endl;      \
  } while(0)

// Create a GPU program i.e., a graphics pipeline
ShaderProgram::ShaderProgram() : m_id(glCreateProgram()) {}


ShaderProgram::~ShaderProgram() {
    glDeleteProgram(m_id);
}

std::string ShaderProgram::file2String(const std::string &filename) {
    std::ifstream input(filename.c_str());
    if (!input)
        throw std::ios_base::failure("[Shader Program][file2String] Error: cannot open " + filename);
    std::stringstream buffer;
    buffer << input.rdbuf();
    return buffer.str();
}

void ShaderProgram::loadShader(GLenum type, const std::string &shaderFilename) {
    GLuint shader = glCreateShader(type);
    // Create the shader, e.g., a vertex shader to be applied to every single vertex of a mesh
    std::string shaderSourceString = file2String(shaderFilename); // Loads the shader source from a file to a C++ string
    if (shaderSourceString.empty()) std::cerr << "Empty shader" << std::endl;
    const auto *shaderSource = (const GLchar *) shaderSourceString.c_str(); // Interface the C++ string through a C pointer
    glShaderSource(shader, 1, &shaderSource, nullptr); // Load the vertex shader source code
    glCompileShader(shader);  // THe GPU driver compile the shader

    GLint isCompiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == GL_FALSE) {
        GLint maxLength = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        std::vector<GLchar> errorLog(maxLength);
        glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);

        std::cerr << shaderFilename << " " << errorLog.data() << std::endl;

        // Provide the infolog in whatever manor you deem best.
        // Exit with failure.
        glDeleteShader(shader); // Don't leak the shader.

        exit(2);
    }

    glAttachShader(m_id, shader); // Set the vertex shader as the one ot be used with the program/pipeline
    glDeleteShader(shader);
}

std::shared_ptr<ShaderProgram> ShaderProgram::genBasicShaderProgram(const std::string &vertexShaderFilename,
                                                                    const std::string &fragmentShaderFilename) {
    std::shared_ptr<ShaderProgram> shaderProgramPtr = std::make_shared<ShaderProgram>();
    shaderProgramPtr->loadShader(GL_VERTEX_SHADER, vertexShaderFilename);
    shaderProgramPtr->loadShader(GL_FRAGMENT_SHADER, fragmentShaderFilename);
    shaderProgramPtr->link();
    shaderProgramPtr->use();

    std::cout << "Shader loaded successfully" << std::endl;
    return shaderProgramPtr;
}


std::shared_ptr<ShaderProgram> ShaderProgram::genGeometryShaderPipeline(const std::string &vertexShaderFilename,
                                                                        const std::string &geometryShaderFilename,
                                                                        const std::string &fragmentShaderFilename) {
    std::shared_ptr<ShaderProgram> shaderProgramPtr = std::make_shared<ShaderProgram>();
    shaderProgramPtr->loadShader(GL_VERTEX_SHADER, vertexShaderFilename);
    TEST_OPENGL_ERROR();
    shaderProgramPtr->loadShader(GL_GEOMETRY_SHADER, geometryShaderFilename);
    TEST_OPENGL_ERROR();
    shaderProgramPtr->loadShader(GL_FRAGMENT_SHADER, fragmentShaderFilename);
    TEST_OPENGL_ERROR();
    shaderProgramPtr->link();
    TEST_OPENGL_ERROR();
    shaderProgramPtr->validate();
    TEST_OPENGL_ERROR();
    shaderProgramPtr->use();
    TEST_OPENGL_ERROR();
    std::cout << "Shader loaded successfully" << std::endl;
    return shaderProgramPtr;
}

std::shared_ptr<ShaderProgram> ShaderProgram::genTessShaderProgram(const std::string &vertexShaderFilename,
                                                                   const std::string &tessControlShaderFilename,
                                                                   const std::string &tessEvalShaderFilename,
                                                                   const std::string &fragmentShaderFilename) {
    std::shared_ptr<ShaderProgram> shaderProgramPtr = std::make_shared<ShaderProgram>();
    shaderProgramPtr->loadShader(GL_VERTEX_SHADER, vertexShaderFilename);
    TEST_OPENGL_ERROR();
    shaderProgramPtr->loadShader(GL_TESS_CONTROL_SHADER, tessControlShaderFilename);
    TEST_OPENGL_ERROR();
    shaderProgramPtr->loadShader(GL_TESS_EVALUATION_SHADER, tessEvalShaderFilename);
    TEST_OPENGL_ERROR();
    shaderProgramPtr->loadShader(GL_FRAGMENT_SHADER, fragmentShaderFilename);
    TEST_OPENGL_ERROR();
    shaderProgramPtr->link();
    TEST_OPENGL_ERROR();
    shaderProgramPtr->validate();
    TEST_OPENGL_ERROR();
    shaderProgramPtr->use();
    TEST_OPENGL_ERROR();
    std::cout << "Shader loaded successfully" << std::endl;

    return shaderProgramPtr;
}

std::shared_ptr<ShaderProgram> ShaderProgram::genFullShaderProgram(const std::string &vertexShaderFilename,
                                                                   const std::string &tessControlShaderFilename,
                                                                   const std::string &tessEvalShaderFilename,
                                                                   const std::string &geometryShaderFilename,
                                                                   const std::string &fragmentShaderFilename) {
    std::shared_ptr<ShaderProgram> shaderProgramPtr = std::make_shared<ShaderProgram>();
    shaderProgramPtr->loadShader(GL_VERTEX_SHADER, vertexShaderFilename);
    TEST_OPENGL_ERROR();
    shaderProgramPtr->loadShader(GL_TESS_CONTROL_SHADER, tessControlShaderFilename);
    TEST_OPENGL_ERROR();
    shaderProgramPtr->loadShader(GL_TESS_EVALUATION_SHADER, tessEvalShaderFilename);
    TEST_OPENGL_ERROR();
    shaderProgramPtr->loadShader(GL_GEOMETRY_SHADER, geometryShaderFilename);
    TEST_OPENGL_ERROR();
    shaderProgramPtr->loadShader(GL_FRAGMENT_SHADER, fragmentShaderFilename);
    TEST_OPENGL_ERROR();
    shaderProgramPtr->link();
    TEST_OPENGL_ERROR();
    shaderProgramPtr->validate();
    TEST_OPENGL_ERROR();
    shaderProgramPtr->use();
    TEST_OPENGL_ERROR();
    std::cout << "Shader loaded successfully" << std::endl;

    return shaderProgramPtr;
}