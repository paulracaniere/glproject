#ifndef MESH_LOADER_H
#define MESH_LOADER_H

#include <string>
#include <memory>

#include "Mesh.h"

namespace MeshLoader {

    /// Loads an OFF mesh file. See https://en.wikipedia.org/wiki/OFF_(file_format)
    void loadOFF(const std::string &filename, const std::shared_ptr<Mesh> &meshPtr);

    /// Loads an OBJ scene file.
    bool loadOBJ(const std::string &filename, const std::shared_ptr<Mesh> &meshPtr, int modelIndex = 0);

}

#endif // MESH_LOADER_H