#ifndef TEST_GL_BCCLOADER_H
#define TEST_GL_BCCLOADER_H

#include <cstdint>
#include <cstdio>
#include <string>
#include <vector>
#include <glm/glm.hpp>

class BCC {
public:
    struct BCCHeader {
        char sign[3];
        unsigned char byteCount;
        char curveType[2];
        char dimensions;
        char upDimension;
        uint64_t curveCount;
        uint64_t totalControlPointCount;
        char fileInfo[40];
    };

    bool loadFromFile(const std::string &file) {
        FILE *pFile = fopen(file.c_str(), "rb");
        if (pFile == nullptr) {
            std::cerr << "Could not read file" << std::endl;
            return false;
        }

        fread(&header, sizeof(header), 1, pFile);

        if (header.sign[0] != 'B') return false; // Invalid file signature
        if (header.sign[1] != 'C') return false; // Invalid file signature
        if (header.sign[2] != 'C') return false; // Invalid file signature
        if (header.byteCount != 0x44) return false; // Only supporting 4-byte integers and floats
        if (header.curveType[0] != 'C') return false; // Not a Catmull-Rom curve
        if (header.curveType[1] != '0') return false; // Not uniform parametrization
        if (header.dimensions != 3) return false; // Only curves in 3D

        controlPoints.resize(header.totalControlPointCount);
        firstControlPoint.resize(header.curveCount + 1);
        isCurveLoop.resize(header.curveCount);

        glm::vec3 *cp = &controlPoints[0];

        int prevCP = 0;
        for (uint64_t i = 0; i < header.curveCount; i++) {
            int curveControlPointCount;
            fread(&curveControlPointCount, sizeof(int), 1, pFile);
            isCurveLoop[i] = curveControlPointCount < 0;
            if (curveControlPointCount < 0) curveControlPointCount = -curveControlPointCount;

            fread(cp, sizeof(glm::vec3), curveControlPointCount, pFile);
            cp += curveControlPointCount;
            firstControlPoint[i] = prevCP;
            prevCP += curveControlPointCount;
        }
        firstControlPoint[header.curveCount] = prevCP;

        return true;
    }

private:
    BCCHeader header;
    std::vector<glm::vec3> controlPoints;
    std::vector<int> firstControlPoint;
    std::vector<char> isCurveLoop;
};


#endif //TEST_GL_BCCLOADER_H
