#include "Skybox.h"

#define STB_IMAGE_IMPLEMENTATION

Skybox::Skybox() = default;

void Skybox::init() {
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    glGenBuffers(1, &m_posVbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_posVbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * m_triangle.size(), m_triangle.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0); // Deactivates the VAO just created. Will be activated at rendering time.
}

void Skybox::loadFaceCubeMap(const std::string &filename, GLenum face) {
    int width, height, numComponents;
    unsigned char *data;
    GLenum format = 0;

    data = stbi_load(filename.c_str(),
                     &width,
                     &height,
                     &numComponents, // 1 for a 8 bit greyscale image, 3 for 24bits RGB image
                     0);
    switch (numComponents) {
        case (1):
            format = GL_RED;
            break;
        case (3):
            format = GL_RGB;
            break;
        default:
            format = GL_RGBA;
            break;
    }

    glTexImage2D(face, // Generates 2D texture in GPU
                 0, // LoD at closest level
                 format, // Number of components of image
                 width,
                 height,
                 0, // Always 0
                 format, // Number of components of image
                 GL_UNSIGNED_BYTE, // Size of each pixel in bits (image encoding)
                 data); // Array/Pointer of data

    // Freeing the now useless CPU memory
    stbi_image_free(data);
}

void Skybox::loadCubeMapTexture(const std::string &right_filename, const std::string &left_filename,
                                const std::string &top_filename, const std::string &bottom_filename,
                                const std::string &back_filename, const std::string &front_filename) {


    glGenTextures(1, &m_tex);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_tex);

    loadFaceCubeMap(right_filename, GL_TEXTURE_CUBE_MAP_POSITIVE_X + 0);
    loadFaceCubeMap(left_filename, GL_TEXTURE_CUBE_MAP_POSITIVE_X + 1);
    loadFaceCubeMap(top_filename, GL_TEXTURE_CUBE_MAP_POSITIVE_X + 2);
    loadFaceCubeMap(bottom_filename, GL_TEXTURE_CUBE_MAP_POSITIVE_X + 3);
    loadFaceCubeMap(back_filename, GL_TEXTURE_CUBE_MAP_POSITIVE_X + 4);
    loadFaceCubeMap(front_filename, GL_TEXTURE_CUBE_MAP_POSITIVE_X + 5);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void Skybox::activateTexture(GLuint index) {
    glActiveTexture(GL_TEXTURE0 + index);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_tex);
}

void Skybox::deactivateTexture(GLuint index) {
    glActiveTexture(GL_TEXTURE0 + index);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void Skybox::render() {
    // Activate the VAO storing geometry data
    glBindVertexArray(m_vao);
    // Binds texture buffer
    activateTexture(0);

    // Call for rendering: stream the current GPU geometry through the current GPU program
    glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(36));

    deactivateTexture(0);
    glBindVertexArray(0);
}

void Skybox::clear() {
    if (m_vao) {
        glDeleteVertexArrays(1, &m_vao);
        m_vao = 0;
    }
    if (m_posVbo) {
        glDeleteBuffers(1, &m_posVbo);
        m_posVbo = 0;
    }
    if (m_tex) {
        glDeleteTextures(1, &m_tex);
        m_tex = 0;
    }
}
