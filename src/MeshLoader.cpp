#include "MeshLoader.h"

#include <iostream>
#include <fstream>
#include <exception>
#include <ios>

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

using namespace std;

void MeshLoader::loadOFF(const std::string &filename, const std::shared_ptr<Mesh> &meshPtr) {
    std::cout << " > Start loading mesh <" << filename << ">" << std::endl;
    meshPtr->clear();
    ifstream in(filename.c_str());
    if (!in)
        throw std::ios_base::failure("[Mesh Loader][loadOFF] Cannot open " + filename);
    string offString;
    unsigned int sizeV, sizeT, tmp;
    in >> offString >> sizeV >> sizeT >> tmp;
    auto &P = meshPtr->vertexPositions();
    auto &T = meshPtr->triangleIndices();
    P.resize(sizeV);
    T.resize(sizeT);
    size_t tracker = (sizeV + sizeT) / 20;
    std::cout << " > [" << std::flush;
    for (unsigned int i = 0; i < sizeV; i++) {
        if (tracker > 0 && i % tracker == 0)
            std::cout << "-" << std::flush;
        in >> P[i][0] >> P[i][1] >> P[i][2];
    }
    int s;
    for (unsigned int i = 0; i < sizeT; i++) {
        if (tracker > 0 && (sizeV + i) % tracker == 0)
            std::cout << "-" << std::flush;
        in >> s;
        for (unsigned int j = 0; j < 3; j++)
            in >> T[i][j];
    }
    std::cout << "]" << std::endl;

    in.close();
    meshPtr->vertexNormals().resize(P.size(), glm::vec3(0.f, 0.f, 1.f));
    meshPtr->computePlanarParameterization();
    meshPtr->recomputePerVertexNormals();
    meshPtr->recomputeTangentSpace();
    std::cout << " > Mesh <" << filename << "> loaded" << std::endl;
}

bool MeshLoader::loadOBJ(const std::string &filename, const std::shared_ptr<Mesh> &meshPtr, int modelIndex) {
    std::cout << " > Start loading mesh <" << filename << ">" << std::endl;
    meshPtr->clear();

    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(filename,
                                             aiProcess_Triangulate |
                                             aiProcess_FlipUVs |
                                             aiProcess_CalcTangentSpace);

    if (!scene || scene->mNumMeshes == 0) {
        throw std::ios_base::failure("[Mesh Loader][loadOBJ] Cannot open " + filename);
    }

    if (modelIndex >= scene->mNumMeshes) {
        return false;
    }
    const aiMesh *mesh = scene->mMeshes[modelIndex];

    if (mesh->mPrimitiveTypes != aiPrimitiveType::aiPrimitiveType_TRIANGLE) {
        throw std::ios_base::failure("[Mesh Loader][loadOBJ] " + filename + " should be made with triangles");
    }

    unsigned int sizeV, sizeT;
    sizeV = mesh->mNumVertices;
    sizeT = mesh->mNumFaces;

    // Vertices
    auto &P = meshPtr->vertexPositions();
    P.resize(sizeV);
    for (unsigned int i = 0; i < sizeV; i++) {
        for (unsigned int j = 0; j < 3; j++) {
            P[i][j] = mesh->mVertices[i][j];
        }
    }
    std::cout << sizeV << " vertices loaded." << std::endl;

    // Polygons
    auto &T = meshPtr->triangleIndices();
    T.resize(sizeT);
    for (unsigned int i = 0; i < sizeT; i++) {
        for (unsigned int j = 0; j < mesh->mFaces->mNumIndices; j++) {
            T[i][j] = mesh->mFaces[i].mIndices[j];
        }
    }
    std::cout << sizeT << " triangles loaded." << std::endl;

    // Normals
    meshPtr->vertexNormals().resize(P.size(), glm::vec3(0.f, 0.f, 1.f));
    if (mesh->HasNormals()) {
        auto &N = meshPtr->vertexNormals();
        for (unsigned int i = 0; i < sizeV; i++) {
            for (unsigned int j = 0; j < 3; j++) {
                N[i][j] = mesh->mNormals[i][j];
            }
        }
        std::cout << "Normals loaded." << std::endl;
    } else {
        meshPtr->recomputePerVertexNormals();
    }

    // UVs
    meshPtr->vertexTexCoords().resize(P.size(), glm::vec2(0.f, 0.f));
    if (mesh->HasTextureCoords(0)) {
        auto &UV = meshPtr->vertexTexCoords();
        for (unsigned int i = 0; i < sizeV; i++) {
            for (unsigned int j = 0; j < 2; j++) {
                UV[i][j] = mesh->mTextureCoords[0][i][j];
            }
        }
        std::cout << "Texture coordinates loaded." << std::endl;
    } else {
        meshPtr->computePlanarParameterization();
    }

    // Binormals and Tangents
    meshPtr->vertexBinormals().resize(P.size(), glm::vec3(0.f, 0.f, 1.f));
    meshPtr->vertexTangents().resize(P.size(), glm::vec3(0.f, 0.f, 1.f));
    if (mesh->HasTangentsAndBitangents()) {
        auto &BN = meshPtr->vertexBinormals();
        auto &TG = meshPtr->vertexTangents();
        for (unsigned int i = 0; i < sizeV; i++) {
            for (unsigned int j = 0; j < 3; j++) {
                BN[i][j] = mesh->mBitangents[i][j];
                TG[i][j] = mesh->mTangents[i][j];
            }
        }
        std::cout << "Tangent spaces loaded." << std::endl;
    } else {
        meshPtr->recomputeTangentSpace();
    }

    std::cout << " > Mesh " << mesh->mName.C_Str() << " from <" << filename << "> loaded" << std::endl;

//    std::cout << material->GetTextureCount(aiTextureType_AMBIENT) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_AMBIENT_OCCLUSION) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_BASE_COLOR) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_DIFFUSE) << std::endl; // map_Kd
//    std::cout << material->GetTextureCount(aiTextureType_DIFFUSE_ROUGHNESS) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_DISPLACEMENT) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_EMISSION_COLOR) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_EMISSIVE) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_HEIGHT) << std::endl; // bump
//    std::cout << material->GetTextureCount(aiTextureType_LIGHTMAP) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_METALNESS) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_NONE) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_NORMAL_CAMERA) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_NORMALS) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_OPACITY) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_REFLECTION) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_SHININESS) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_SPECULAR) << std::endl;
//    std::cout << material->GetTextureCount(aiTextureType_UNKNOWN) << std::endl << std::endl;

    return true;
}
