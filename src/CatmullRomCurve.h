#ifndef TEST_GL_CATMULLROMCURVE_H
#define TEST_GL_CATMULLROMCURVE_H

#include <vector>
#include <glm/glm.hpp>
#include <glad/glad.h>
#include <string>


class CatmullRomCurve {
public:
    ~CatmullRomCurve() { clear(); };

    void init();

    void render(int nb_vertices = -1);

    void clear();

    void loadSimpleExample();

    void loadLineSample();

    bool loadFromBCCFile(const std::string &path);

    void computeBoundingSphere(glm::vec3 &center, float &radius) const;

    void activateTextures(GLuint index) const;

    void deactivateTexture(GLuint index) const;

private:
    void computeIbo();

    void computeProgression();

    void computeRadiiAndInitialAngles();

    std::vector<glm::vec3> m_controlPoints;
    std::vector<GLchar> m_isLoop;
    std::vector<GLuint> m_firstControlPoint;
    std::vector<glm::uvec4> m_indices;
    std::vector<GLfloat> m_progression;
    std::vector<glm::vec2> m_radiiAndAngles;
    GLuint m_vao = 0;
    GLuint m_posVbo = 0;
    GLuint m_progVbo = 0;
    GLuint m_ibo = 0;
    GLuint m_radiiAnglesTex = 0;
};


#endif //TEST_GL_CATMULLROMCURVE_H
