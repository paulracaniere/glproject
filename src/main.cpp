#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "ShaderProgram.h"
#include "MeshLoader.h"
#include "Skybox.h"
#include "Mesh.h"
#include "Camera.h"
#include "CatmullRomCurve.h"
#include "Shadow.h"
#include "LEADR.h"

#define TEST_OPENGL_ERROR()                                                             \
  do {                                    \
    GLenum err = glGetError();                                            \
    if (err != GL_NO_ERROR) std::cerr << "OpenGL ERROR! #"<< err <<" at line "<< __LINE__ <<" "<<__FILE__<< std::endl;      \
  } while(0)

GLFWwindow *windowPtr = nullptr;
//std::shared_ptr<ShaderProgram> shaderProgramPtr;
//std::shared_ptr<ShaderProgram> geomShaderProgramPtr;
std::shared_ptr<ShaderProgram> skyboxShaderProgramPtr;
//std::shared_ptr<ShaderProgram> catromShaderProgramPtr;
//std::shared_ptr<ShaderProgram> catromShadowShaderProgramPtr;
std::shared_ptr<ShaderProgram> LEADRShaderProgramPtr;
std::shared_ptr<Camera> cameraPtr;
std::vector<std::shared_ptr<Mesh>> meshPtrList;
//std::shared_ptr<CatmullRomCurve> catromCurve;
//std::shared_ptr<Shadow> catromShadow;
std::shared_ptr<LEADR> leadrPtr;
std::shared_ptr<Skybox> skyboxPtr;
static const std::string DEFAULT_FILENAME("OpenGL");
static const std::string SHADER_PATH("Resources/Shaders/");

static bool isRotating;
static bool isPanning;
static bool isZooming;
static bool isStretching;
static double baseX;
static double baseY;
static glm::vec3 baseRot;
static glm::vec3 baseTrans;
static float baseStretch;
static float meshScale;
static GLfloat anim_time;
static bool fill_mode;
static float tess_level;
static float LEADR_scale_level;
static float LEADR_repeat;
static float LEADR_stretch;
static int LEADR_example;
static std::vector<GLfloat> LEADR_samplingWeights;
static std::vector<GLfloat> LEADR_samplingPositions;
//static float catmullRomWidth;
//static int nb_plies;
//static int nb_fibers_per_ply;
//static bool isShadowOn;

enum DISPLAY_MODE {
    DISPLAY_THREAD = 0,
    DISPLAY_POSE = 1,
    DISPLAY_ALT = 2
};
static DISPLAY_MODE mode;

/// Initiates "not const" global variables
void initGlobalVariables() {
    isRotating = false;
    isPanning = false;
    isZooming = false;
    isStretching = false;
    baseX = 0.0;
    baseY = 0.0;
    baseRot = glm::vec3(0.0, 0.0, 0.0);
    baseTrans = glm::vec3(0.0, 0.0, 0.0);
    baseStretch = 1.0f;
    meshScale = 1.0;
    anim_time = 0.0f;
    tess_level = 1.0f;
    LEADR_scale_level = 0.1f;
    LEADR_repeat = 1.0f;
    LEADR_stretch = 1.0f;
    LEADR_samplingWeights = std::vector<GLfloat>();
    LEADR_samplingPositions = std::vector<GLfloat>();
//    catmullRomWidth = 2e-3f;
//    nb_plies = 3;
//    nb_fibers_per_ply = 21;
//    isShadowOn = false;
    mode = DISPLAY_THREAD;
}

/// Executed each time the window is resized. Adjust the aspect ratio and the rendering viewport to the current window.
void windowSizeCallback(GLFWwindow *, int width, int height) {
    cameraPtr->setAspectRatio(static_cast<float>(width) / static_cast<float>(height));
    glViewport(0, 0, (GLint) width, (GLint) height); // Dimension of the rendering region in the window
}

/// Called each time a mouse button is pressed
void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS && mods == GLFW_MOD_CONTROL) {
        if (!isStretching) {
            isStretching = true;
            glfwGetCursorPos(window, &baseX, &baseY);
            baseStretch = LEADR_stretch;
        }
    } else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        if (!isRotating) {
            isRotating = true;
            glfwGetCursorPos(window, &baseX, &baseY);
            baseRot = cameraPtr->getRotation();
        }
    } else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
        isRotating = false;
        isStretching = false;
    } else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {
        if (!isPanning) {
            isPanning = true;
            glfwGetCursorPos(window, &baseX, &baseY);
            baseTrans = cameraPtr->getTranslation();
        }
    } else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE) {
        isPanning = false;
    } else if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS) {
        if (!isZooming) {
            isZooming = true;
            glfwGetCursorPos(window, &baseX, &baseY);
            baseTrans = cameraPtr->getTranslation();
        }
    } else if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_RELEASE) {
        isZooming = false;
    }
}

/// Called each time the mouse cursor moves
void cursorPosCallback(GLFWwindow *, double x_position, double y_position) {
    int width, height;
    glfwGetWindowSize(windowPtr, &width, &height);
    auto normalizer = static_cast<double> ((width + height) / 2.0);
    auto dx = static_cast<double> ((baseX - x_position) / normalizer);
    auto dy = static_cast<double> ((y_position - baseY) / normalizer);
    if (isRotating) {
        glm::vec3 dRot(-dy * M_PI, dx * M_PI, 0.0);
        cameraPtr->setRotation(baseRot + dRot);
    } else if (isPanning) {
        cameraPtr->setTranslation(baseTrans + meshScale * glm::vec3(dx, dy, 0.0));
    } else if (isZooming) {
        cameraPtr->setTranslation(baseTrans + meshScale * glm::vec3(0.0, 0.0, dy));
    } else if (isStretching) {
        LEADR_stretch = baseStretch - float(dx);
    }
}

/// Called each time the mouse scroller is activated
void mouseScrollCallback(GLFWwindow *window, double, double y_offset) {
    baseTrans = cameraPtr->getTranslation();
    glfwGetCursorPos(window, &baseX, &baseY);

    int width, height;

    glfwGetWindowSize(windowPtr, &width, &height);
    auto normalizer = static_cast<float> ((width + height) / 2.0);
    auto dy = static_cast<float> (y_offset / normalizer * 10.);

    cameraPtr->setTranslation(baseTrans + meshScale * glm::vec3(0.0, 0.0, dy));
}

/// Called each time the keyboard is activated
void keyCallback(GLFWwindow *, int key, int, int action, int) {
    if (key == GLFW_KEY_ENTER && action == GLFW_PRESS) {
        if (!fill_mode) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            fill_mode = true;
        } else {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            fill_mode = false;
        }
//    } else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
//        if (isShadowOn) {
//            std::cout << "Hiding shadows" << std::endl;
//            isShadowOn = false;
//        } else {
//            std::cout << "Showing shadows" << std::endl;
//            isShadowOn = true;
//        }
    } else if ((key <= GLFW_KEY_KP_9 && key >= GLFW_KEY_KP_0) && action == GLFW_PRESS) {
        LEADR_example = key - GLFW_KEY_KP_0;
    } else if ((key <= GLFW_KEY_9 && key >= GLFW_KEY_0) && action == GLFW_PRESS) {
        LEADR_example = key - GLFW_KEY_0;
//    } else if ((key == GLFW_KEY_KP_0 || key == GLFW_KEY_0) && action == GLFW_PRESS) {
//        catromShaderProgramPtr = ShaderProgram::genFullShaderProgram(
//                SHADER_PATH + "CatmullRom/VertexShader.glsl",
//                SHADER_PATH + "CatmullRom/TessControlShader.glsl",
//                SHADER_PATH + "CatmullRom/TessEvalShader.glsl",
//                SHADER_PATH + "CatmullRom/GeometryShader.glsl",
//                SHADER_PATH + "CatmullRom/FragmentShader.glsl");
//        mode = DISPLAY_THREAD;
//    } else if ((key == GLFW_KEY_KP_1 || key == GLFW_KEY_1) && action == GLFW_PRESS) {
//        catromShaderProgramPtr = ShaderProgram::genFullShaderProgram(
//                SHADER_PATH + "CatmullRom/VertexShader.glsl",
//                SHADER_PATH + "CatmullRom/TessControlShader.glsl",
//                SHADER_PATH + "CatmullRom/TessEvalShader.glsl",
//                SHADER_PATH + "CatmullRom/GeometryShader_pose.glsl",
//                SHADER_PATH + "CatmullRom/FragmentShader.glsl");
//        mode = DISPLAY_POSE;
//    } else if ((key == GLFW_KEY_KP_2 || key == GLFW_KEY_2) && action == GLFW_PRESS) {
//        catromShaderProgramPtr = ShaderProgram::genFullShaderProgram(
//                SHADER_PATH + "CatmullRomShadow/VertexShader.glsl",
//                SHADER_PATH + "CatmullRomShadow/TessControlShader.glsl",
//                SHADER_PATH + "CatmullRomShadow/TessEvalShader.glsl",
//                SHADER_PATH + "CatmullRomShadow/GeometryShader.glsl",
//                SHADER_PATH + "CatmullRomShadow/FragmentShader.glsl");
//        mode = DISPLAY_ALT;
    } else if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
        tess_level = (tess_level == 1) ? 2 : floorf(tess_level * 1.5f);
        std::cout << "Tessellation level set to: " << tess_level << std::endl;
    } else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
        tess_level = floorf(tess_level * 0.66f);
        if (tess_level == 0.0f) tess_level = 1.0f;
        std::cout << "Tessellation level set to: " << tess_level << std::endl;
//    } else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
//        catmullRomWidth = catmullRomWidth * 1.2f;
//        std::cout << "C-R width set to: " << catmullRomWidth << std::endl;
//    } else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
//        catmullRomWidth = catmullRomWidth / 1.2f;
//        std::cout << "C-R width set to: " << catmullRomWidth << std::endl;
    } else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
        LEADR_scale_level = LEADR_scale_level * 1.5f;
        std::cout << "LEADR scale level set to: " << LEADR_scale_level << std::endl;
    } else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
        LEADR_scale_level = LEADR_scale_level * 0.66f;
        if (LEADR_scale_level == 0.0f) LEADR_scale_level = 1.0f;
        std::cout << "LEADR scale level set to: " << LEADR_scale_level << std::endl;
    } else if (key == GLFW_KEY_Z && action == GLFW_PRESS) {
        LEADR_repeat += 1.0f;
        std::cout << "LEADR repeating level set to: " << LEADR_repeat << std::endl;
    } else if (key == GLFW_KEY_S && action == GLFW_PRESS) {
        LEADR_repeat = std::fmax(1.0f, LEADR_repeat - 1.0f);
        std::cout << "LEADR repeating level set to: " << LEADR_repeat << std::endl;
    }
}

/// Clears all loaded buffers and windows
void clear() {
    cameraPtr.reset();
    for (auto &meshPtr : meshPtrList) {
        meshPtr.reset();
    }
//    catromCurve.reset();
//    catromShadow.reset();
    skyboxPtr.reset();
    leadrPtr.reset();
//    shaderProgramPtr.reset();
//    geomShaderProgramPtr.reset();
    skyboxShaderProgramPtr.reset();
//    catromShaderProgramPtr.reset();
//    catromShadowShaderProgramPtr.reset();
    LEADRShaderProgramPtr.reset();
    glfwDestroyWindow(windowPtr);
    glfwTerminate();
}

/// Exits with given critical error message
void exitOnCriticalError(const std::string &message) {
    std::cerr << "> [Critical error]" << message << std::endl;
    std::cerr << "> [Clearing resources]" << std::endl;
    clear();
    std::cerr << "> [Exit]" << std::endl;
    std::exit(EXIT_FAILURE);
}

/// Inits the OpenGL window
void initGLFW(const std::string &windowTitle = "3D Model") {
    // Initialize GLFW, the library responsible for window management
    if (!glfwInit()) {
        std::cerr << "ERROR: Failed to init GLFW" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    // Before creating the window, set some option flags
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    // Create the window
    windowPtr = glfwCreateWindow(500, 500, windowTitle.c_str(), nullptr, nullptr);
    if (!windowPtr) {
        std::cerr << "ERROR: Failed to open window" << std::endl;
        glfwTerminate();
        std::exit(EXIT_FAILURE);
    } else {
        std::cout << "SUCCESS: Window opened" << std::endl;
    }

    // Load the OpenGL context in the GLFW window using GLAD OpenGL wrangler
    glfwMakeContextCurrent(windowPtr);

    // Connect the callbacks for interactive control
    glfwSetWindowSizeCallback(windowPtr, windowSizeCallback);
    glfwSetCursorPosCallback(windowPtr, cursorPosCallback);
    glfwSetMouseButtonCallback(windowPtr, mouseButtonCallback);
    glfwSetScrollCallback(windowPtr, mouseScrollCallback);
    glfwSetKeyCallback(windowPtr, keyCallback);
}

/// Inits OpenGL by loading functions from GLAD and sets parameters
void initOpenGL() {
    // Load extensions for modern OpenGL
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
        exitOnCriticalError("[Failed to initialize OpenGL context]");

    glCullFace(GL_BACK);     // Specifies the faces to cull (here the ones pointing away from the camera)
    glEnable(GL_CULL_FACE); // Enables face culling (based on the orientation defined by the CW/CCW enumeration).
    glDepthFunc(GL_LESS); // Specify the depth test for the z-buffer
    glEnable(GL_DEPTH_TEST); // Enable the z-buffer test in the rasterization
    fill_mode = true;
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
//    glEnable(GL_BLEND); // Alpha channels
//    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // Specify the background color, used any time the framebuffer is cleared
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    // Loads and compile the programmable shader pipeline
    try {
//        catromShaderProgramPtr = ShaderProgram::genFullShaderProgram(SHADER_PATH + "CatmullRom/VertexShader.glsl",
//                                                                     SHADER_PATH + "CatmullRom/TessControlShader.glsl",
//                                                                     SHADER_PATH + "CatmullRom/TessEvalShader.glsl",
//                                                                     SHADER_PATH + "CatmullRom/GeometryShader.glsl",
//                                                                     SHADER_PATH + "CatmullRom/FragmentShader.glsl");
//
//        catromShadowShaderProgramPtr = ShaderProgram::genFullShaderProgram(
//                SHADER_PATH + "CatmullRomShadow/VertexShader.glsl",
//                SHADER_PATH + "CatmullRomShadow/TessControlShader.glsl",
//                SHADER_PATH + "CatmullRomShadow/TessEvalShader.glsl",
//                SHADER_PATH + "CatmullRomShadow/GeometryShader.glsl",
//                SHADER_PATH + "CatmullRomShadow/FragmentShader.glsl");

        LEADRShaderProgramPtr = ShaderProgram::genTessShaderProgram(SHADER_PATH + "LEADR/VertexShader.glsl",
                                                                    SHADER_PATH + "LEADR/TessControlShader.glsl",
                                                                    SHADER_PATH + "LEADR/TessEvalShader.glsl",
                                                                    SHADER_PATH + "LEADR/FragmentShader.glsl");

        skyboxShaderProgramPtr = ShaderProgram::genBasicShaderProgram(SHADER_PATH + "skybox/VertexShader.glsl",
                                                                      SHADER_PATH + "skybox/FragmentShader.glsl");

        TEST_OPENGL_ERROR();
    } catch (std::exception &e) {
        exitOnCriticalError(std::string("[Error loading shader program]") + e.what());
    }
}

/// Inits Geometry
void initScene(const std::string &meshFilename) {
    // Camera
    int width, height;
    glfwGetWindowSize(windowPtr, &width, &height);
    cameraPtr = std::make_shared<Camera>();
    cameraPtr->setAspectRatio(static_cast<float>(width) / static_cast<float>(height));

    // Mesh
    try {
        std::shared_ptr<Mesh> mesh;

        skyboxPtr = std::make_shared<Skybox>();
        std::string baseCubeMapFolder = "Resources/Skybox/bleak/";
        skyboxPtr->loadCubeMapTexture(baseCubeMapFolder + "right.tga", baseCubeMapFolder + "left.tga",
                                      baseCubeMapFolder + "up4.tga", baseCubeMapFolder + "down3.tga",
                                      baseCubeMapFolder + "back.tga", baseCubeMapFolder + "front.tga");

        mesh = Mesh::create_area(10);
//        mesh = Mesh::create_sphere(100);
        mesh->loadDiffuseMap_8bit("Resources/Albedos/rocks.jpg");
        meshPtrList.push_back(mesh);

        TEST_OPENGL_ERROR();

        leadrPtr = std::make_shared<LEADR>();
//        leadrPtr->loadHeightMap_16bit("Resources/HeightMap/norway.png");
//        leadrPtr->loadHeightMap_8bit("Resources/HeightMap/displacement.jpg");
        leadrPtr->loadHeightMap_8bit("Resources/HeightMap/rocks.jpg");
//        leadrPtr->generateSinusoidalTexture(4096);

        leadrPtr->sampling(5, 1.8f, LEADR_samplingPositions, LEADR_samplingWeights);

        TEST_OPENGL_ERROR();

//        // Catmull-Rom curve
//        catromCurve = std::make_shared<CatmullRomCurve>();
//        catromCurve->loadLineSample();
//        catromCurve->loadSimpleExample();
//        catromCurve->loadFromBCCFile("Resources/BCC/cable_work_pattern.bcc");
//        catromCurve->loadFromBCCFile("Resources/BCC/glove.bcc");
//        catromCurve->loadFromBCCFile("Resources/BCC/sweater_flame_ribbing.bcc");

//        catromShadow = std::make_shared<Shadow>();

    } catch (std::exception &e) {
        exitOnCriticalError(std::string("[Error loading mesh]") + e.what());
    }
    for (auto &mesh : meshPtrList) {
        mesh->init();
    }
    skyboxPtr->init();
//    catromCurve->init();
//    catromShadow->set_shadow_buffer(1920, 1080);

    // Adjust the camera to the actual mesh
    glm::vec3 center;
    meshPtrList[0]->computeBoundingSphere(center, meshScale);
//    catromCurve->computeBoundingSphere(center, meshScale);

    cameraPtr->setTranslation(center + glm::vec3(0.0, 0.0, 3.0 * meshScale));
    cameraPtr->setNear(meshScale / 100.f);
    cameraPtr->setFar(60.f * meshScale);

    // TODO: Init light to compute MVP for shadow
}

/// The main rendering call
void render() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Erase the color and z buffers.

    TEST_OPENGL_ERROR();
//    shaderProgramPtr->use(); // Activate the program to be used for upcoming primitive
    TEST_OPENGL_ERROR();

    // Compute the projection matrix of the camera and pass it to the GPU program
    glm::mat4 projectionMatrix = cameraPtr->computeProjectionMatrix();

    glm::mat4 modelMatrix = glm::mat4(1.0f); //FIXME: Account for object model matrix

    glm::mat4 viewMatrix = cameraPtr->computeViewMatrix();
    glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;
    glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelViewMatrix));
    TEST_OPENGL_ERROR();

    // Skybox
    skyboxShaderProgramPtr->use();
    glDepthMask(GL_FALSE);
    skyboxShaderProgramPtr->set("projectionMat", projectionMatrix);
    skyboxShaderProgramPtr->set("viewMat", glm::mat4(glm::mat3(viewMatrix)));
    skyboxShaderProgramPtr->set("cubemap", 0);
    skyboxPtr->render();
    skyboxShaderProgramPtr->stop();
    glDepthMask(GL_TRUE);

    TEST_OPENGL_ERROR();

    LEADRShaderProgramPtr->use();
    LEADRShaderProgramPtr->set("projectionMat", projectionMatrix);
    LEADRShaderProgramPtr->set("modelViewMat", modelViewMatrix);
    LEADRShaderProgramPtr->set("tess_level", tess_level);
    LEADRShaderProgramPtr->set("LEADR_scale_level", LEADR_scale_level);
    LEADRShaderProgramPtr->set("LEADR_repeat", LEADR_repeat);
    LEADRShaderProgramPtr->set("LEADR_stretch", LEADR_stretch);
    LEADRShaderProgramPtr->set("LEADR_example", LEADR_example);
    LEADRShaderProgramPtr->set("normalMat", normalMatrix);
    LEADRShaderProgramPtr->set("samplingSize", 5);
    LEADRShaderProgramPtr->set("samplingWeights", LEADR_samplingWeights);
    LEADRShaderProgramPtr->set("samplingPosition", LEADR_samplingPositions);
    LEADRShaderProgramPtr->set("heightTex", 0);
    LEADRShaderProgramPtr->set("LEADRTex0", 1);
    LEADRShaderProgramPtr->set("LEADRTex1", 2);
    leadrPtr->activateTextures(0);
    LEADRShaderProgramPtr->set("cubemapTex", 3);
    skyboxPtr->activateTexture(3);
    LEADRShaderProgramPtr->set("albedoTex", 4);
    meshPtrList[0]->activateTextures(4);
    TEST_OPENGL_ERROR();
    for (const auto &mesh : meshPtrList) {
        mesh->render();
    }
    leadrPtr->deactivateTextures(0);
    skyboxPtr->deactivateTexture(3);
    meshPtrList[0]->deactivateTextures(4);
    LEADRShaderProgramPtr->stop();

    TEST_OPENGL_ERROR();

//    // Shadow management
//    catromShadowShaderProgramPtr->use();
//
//    // Compute the MVP matrix from the light's point of view (FIXME)
//    glm::vec3 lightInvDir = glm::vec3(0.5f, 2.0f, 2.0f);
//    glm::mat4 depthViewMatrix = glm::lookAt(lightInvDir, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
//    glm::mat4 depthModelMatrix = glm::mat4(1.0);
//
//    // Basic example
//    glm::mat4 depthProjectionMatrix = glm::ortho<float>(-2.0f, 2.0f, -2.0f, 2.0f, 0.0f, 5.0f);
//
//    // Pillow
//    glm::mat4 depthProjectionMatrix = glm::ortho<float>(-25.0f, 30.0f, -20.0f, 20.0f, -20.0f, 20.0f);
//
//    // Pullover
//    glm::mat4 depthProjectionMatrix = glm::ortho<float>(-50.0f, 50.0f, -28.0f, 25.0f, -15.0f, 50.0f);
//
//    catromShadowShaderProgramPtr->set("projectionMat", depthProjectionMatrix);
//    catromShadowShaderProgramPtr->set("modelViewMat", depthViewMatrix * depthModelMatrix);
//    catromShadowShaderProgramPtr->set("tess_uniform", tess_level);
//    catromShadowShaderProgramPtr->set("thickness", catmullRomWidth);
//    catromShadowShaderProgramPtr->set("nb_plies", nb_plies);
//
//    catromShadow->bind_buffer();
//    catromCurve->render();
//    catromShadow->unbind_buffer();
//    catromShadowShaderProgramPtr->stop();
//
//    // Line example
//    glm::mat4 projectionMatrixForLine = glm::ortho<float>(-1.0f, 1.0f, -0.1f, 0.1f, 5.5f, 6.5f);
//
//    TEST_OPENGL_ERROR();
//    catromShaderProgramPtr->use();
//    catromShaderProgramPtr->set("isShadowOn", isShadowOn);
//    catromShaderProgramPtr->set("lightInvDir", glm::normalize(lightInvDir));
//    catromShaderProgramPtr->set("nb_plies", nb_plies);
//    catromShaderProgramPtr->set("nb_fibers_per_ply", nb_fibers_per_ply);
//    catromShaderProgramPtr->set("depthProjectionMat", depthProjectionMatrix);
//    catromShaderProgramPtr->set("depthModelViewMat", depthViewMatrix * depthModelMatrix);
//    catromShaderProgramPtr->set("projectionMat", projectionMatrix);
//    catromShaderProgramPtr->set("modelViewMat", modelViewMatrix);
//    catromShaderProgramPtr->set("projectionMat", projectionMatrixForLine);
//    catromShaderProgramPtr->set("modelViewMat", modelViewMatrix);
//    catromShaderProgramPtr->set("projectionMat", depthProjectionMatrix);
//    catromShaderProgramPtr->set("modelViewMat", depthViewMatrix * depthModelMatrix);
//    catromShaderProgramPtr->set("normalMat", normalMatrix);
//    catromShaderProgramPtr->set("tess_uniform", tess_level);
//    catromShaderProgramPtr->set("thickness", catmullRomWidth);
//    catromShaderProgramPtr->set("display_mode", mode);
//    catromShadow->activateTexture(0);
//    catromShaderProgramPtr->set("shadowTex", 0);
//    catromCurve->activateTextures(1);
//    catromShaderProgramPtr->set("radiiAnglesTex", 1);
//
//    catromCurve->render();
//
//    catromShadow->deactivateTexture(0);
//    catromShaderProgramPtr->stop();
    TEST_OPENGL_ERROR();
}

void counttime() {
    double lastTime = glfwGetTime();
    int nbFrames = 0;
        // Measure speed
        double currentTime = glfwGetTime();

            printf("%f ms/frame\n", 1000.0/double(nbFrames));
}

void init(const std::string &meshFilename) {
    initGlobalVariables();
    initGLFW(meshFilename); // Windowing system
    initOpenGL(); // OpenGL Context and shader pipeline
    initScene(meshFilename); // Actual scene to render
}

int main(int argc, char *argv[]) {
    init(DEFAULT_FILENAME);
    std::cout << "OpenGL version: " << GLVersion.major << "." << GLVersion.minor << std::endl;

    while (!glfwWindowShouldClose(windowPtr)) {
        double time = glfwGetTime();
        render();
        glfwSwapBuffers(windowPtr);
        glfwPollEvents();
        glfwSetWindowTitle(windowPtr, ("Framerate: " + std::to_string(1.0 / (glfwGetTime() - time)) + " fps").c_str());
        anim_time += 0.1f;
    }

    return 0;
}
